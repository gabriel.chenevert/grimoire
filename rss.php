<?php

include "php/Main.php";

$new = unserialize(file_get_contents("data/cre.dat"));
$songs = read("songs.dat");

$html = "";

for ($i = 0; $i < 10; $i++) {

  $t = key($new);
  $tim = current($new);

  $song = read("$t/meta.dat");
  $cur = array_key_last($song["versions"]);

  $auth = ($song["versions"][$cur]["qui"] == "hug") ? "Hug" : "Gab";

  $repl = Array(
            "ART" => str_replace("&", "&amp;", $songs[$t]->artist),
            "TIT" => str_replace("&", "&amp;", $songs[$t]->title),
            "URL" => URL . "/?q=$t",
            "AUTH" => "<![CDATA[$auth]]>",
            "DATE" => date("r", strtotime($cur)),
            "TXT" => htmlspecialchars("<pre>".substr(read("$t/$cur.txt"),0,400)."...</pre>")
          );

  $html .= html_fragment("rss_item", $repl);

  next($new);

}

echo html_fragment("rss", array( "URL" => URL, "FEED" => $html));

?>
