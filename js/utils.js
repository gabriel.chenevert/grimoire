function curseur(textarea) {

 var txt = textarea.value + "\n";
 var lignes = txt.split("\n");
 var j = textarea.selectionStart + 1;
 var m = lignes.length;

 for(i = 0; i < m; i++) {
  len = lignes[i].length + 1;
  if(j <= len) break;
  j -= len;
 }

 return [i+1, j, m-1];

}

function tabu(n, strings) {

 txt = "";

 for (i = strings.length - 1; i >= 0; i--) {
  txt += strings[i] + " ";
   for (j = 0; j < n; j++) {
    txt += "-";
   }
   if (i) txt += "\n";
 }

 insertAtCaret("txt", txt);

}

function insert(c) {
  insertAtCaret("txt", c);
}

function insertAtCaret(areaId, text) {
    var txtarea = document.getElementById(areaId);
    var scrollPos = txtarea.scrollTop;
    var caretPos = txtarea.selectionStart;

    var front = (txtarea.value).substring(0, caretPos);
    var back = (txtarea.value).substring(txtarea.selectionEnd, txtarea.value.length);
    txtarea.value = front + text + back;
    caretPos = caretPos + text.length;
    txtarea.selectionStart = caretPos;
    txtarea.selectionEnd = caretPos;
    txtarea.focus();
    txtarea.scrollTop = scrollPos;
}

function toggle_display(el) {
  
  var mode = (el.className=="attach") ? "inline" : "block";
  
  if (el.className=="pop" && el.style.display != "block") {
  
    document.querySelectorAll('.pop').forEach( (e,k) => { e.style.display = "none"; } )
    
  }

  el.style.display = (el.style.display == mode) ? "none" : mode;
  
}
