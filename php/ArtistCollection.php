<?php

// *********************************************************************
//
//                         ArtistCollection
////
// array $this ( str $q => Artist $art )
//
// methods:
//
//   void __construct( str $raw )
//   void sort()
//   str mmark()
//   bool check()
//   void set_artist( str $art, str $raw )
//   void update_only()
//   ArtistCollection startswith($q)
//   ArtistCollection ofcountry($q)
//   ArtistCollection contains($q)
//
// *********************************************************************

class ArtistCollection extends GenericCollection {

  function __construct($raw = null) {
  
    if (is_string($raw)) {
      
      $new_artist = true;
  
      foreach (explode("\n", $raw) as $line) {
      
        $pre = array("* ", "- ", "> ", "@ ");
      
        if (!$line || in_array(substr($line, 0, 2), $pre))
          $new_artist = false;
      
        if ($new_artist && $raw_artist) {        
          $art = new Artist($raw_artist);
          $this[$art->q()] = $art;
          $raw_artist = "";
        }

        $raw_artist .= $line . "\n";
        $new_artist = true;
        
      }
      
      $art = new Artist($raw_artist);
      $this[$art->q()] = $art;
      
    }
  
  }
  
  function mmark() {
    
    $txt = "";
    
    foreach( $this as $q => $art ) $txt .= $art->mmark();
      
    return trim($txt) . "\n";
    
  }

  function check() {

    $ok = true;

    foreach($this as $q => $art)
      if ($art->q() != $q) {
        $ok = false;
        error( $art->q() . " != " . $q . "<br>" );
      }
    return $ok;

  }

  function sort() {

    $this->uasort("art_cmp");
  
  }

  function startswith($c) {

    $res = new ArtistCollection();

    foreach ($this as $q => $art) {
      $q = strval($q);
      if (strtoupper($q[0]) == $c || $c == "#" && (is_numeric($q[0]) || $q == "44") )
        $res[$q] = $art;
    }

    return $res;

  }

  function ofcountry($c) {

    $res = new ArtistCollection();

    foreach ($this as $q => $art)
      if ($art->nat == $c || ($c == "xx" && !$art->nat))
        $res[$q] = $art;

    return $res;

  }

  function contains($q) {

    $res = new ArtistCollection();

    $needles = explode(" ", str_replace(",", " ", $q));

    foreach ($this as $a => $artist) {

      $found = 1;
      $haystack = " ".strtolower(accents($artist->name));

      foreach ($needles as $needle) if ($needle) {
        $chiche = $found && strpos($haystack, strtolower(accents($needle)));
        $found = $chiche;
      }

    if ($found) $res[$a] = $artist;

   }

    return $res;

  }


  function html($title="", $sortname=true, $top=false) {

    $html = "";

    if ($title) $html .= "<h3>" . $title . "</h3>";
    else $html .= "<h3>J'en ai trouvé " . count($this) . "</h3>";

    $html .= '<table id="art"><tr><td>';

    $html .= ( ($top) ? "<ol>" : "<ul>" ) . ENDL;

    $N = ceil(count($this)/4); if ($N < 5) $N = count($this);

    $i = 0;

    foreach($this as $q => $art) {

      if ($i++ == $N) {
        $html .= "</ul></td><td><ul>";
        $i = 1;
      }

      $html .= "  <li>";
      if ($art->big()) $html .= "<b>";
      $name = ($sortname) ? $art->sortname() : $art->name;
      $blue = (superuser() && $art->name == "The Beatles") ? ' class="blue"' : "";
      $html .='<a href="?q=' . $q . '"' . $blue . '>' . $name . '</a>';
      if (nom() == "Gab" && $art->name == "The Beatles")
	$html .= '</span>';
      if ($art->big()) $html .= "</b>";

    }

    if (count($this) == 0) $html .= "  Meilleure chance la prochaine fois\n";

    $html .= ($top) ? " </ol>" : " </ul>";

    $html .= "</td></tr></table>";

    return $html;

  }

  function artist_id( $name ) {
  
    // default
    $try = q_encode(prepo($name));
    if (isset($this[$try])) return $try;
    
    // Nom_Prenom
    $try = q_encode(renversi($name));
    if (isset($this[$try])) return $try;
    
    // au pire
    foreach ($this as $q => $art)
      if ($art->name == $name) return $q;
    
    // not found
    return "FUCK";
    
  }

  function set_artist($art, $data) {
  
    $this[$art] = $data;
    $this->update_only($art);
  
  }

  function update_only($art) {
    
    $this[$art]->update();
    $this->sort();
    $this->save();
    
  }

}

?>
