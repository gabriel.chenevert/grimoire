#!/usr/bin/php
<?php

// sudo su -c "crontab -e" www-data -s /bin/bash
//
// 0 6 * * * /var/www/grimoire/php/daily.php >> /var/www/grimoire/data/cron.log 2>&1

require "Main.php";

echo "[" . date("Y-m-d H:i:s") . "] Starting daily cronjob" . ENDL;

chdir("/data/Public/krab/grimoire");

$artists = read("artists.dat");
$songs = read("songs.dat");

daily();

?>
