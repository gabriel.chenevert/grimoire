<?php

// *********************************************************************
//
//                               Album
//
// str title
// str year
// str a
//
// void html()
//
// *********************************************************************

class Album extends SongCollection {

  function __construct($a, $title = "") {
    
    $this->a = $a;
    if ($title) $this->title = $title;

  }

  function html() {

    $id = isset($this->title) ? q_encode($this->title, "album") : "";
    $full_id = $this->a . "__" . $id;

    $html = "";

    if (isset($this->title)) { // gros
      $title = (strlen($id) > 27) ? iconv_substr($this->title, 0, 22)."&hellip;" : $this->title;

      $html .= ' <a name="' . $id . '" title="' . $this->title . '" ';
      if (superuser()) $html .= 'href="?q=pochette&s=' . $this->a . '__' . $id .'" ';
      $html .= '>';
      if (file_exists("media/pochettes/$full_id.jpg")) $html .= "\n <center><img src=\"media/pochettes/$full_id.jpg\" style=\"width: 115px; height: 115px;\"></center>\n";
      $year = (isset($this->year)) ? $this->year : "&nbsp;&nbsp;&nbsp;&nbsp;";
      $html .= " <h4>$title</a><br><small>" . $year . "</small></h4>\n";
    }

    $html .= " <ul>\n";
    foreach ($this as $n => $song)
      $html .= $song->html($this);

    $html .= " </ul>\n";
    
    return $html;
    
  }

  function mmark() {
  
    $txt = "";
      
    if (isset($this->title)) {
      
      $txt .= "* " . $this->title;
      
      if (isset($this->year)) $txt .= " {" . $this->year . "}";
      
      $txt .= ENDL . ENDL;

    }
    
    foreach ($this as $toune) $txt .= $toune->mmark("album");
    
    $txt .= ENDL;
    
    return $txt;
   
  }



}
