<?php

// *********************************************************************
//
//                       SongListCollection
//
// void html()
//
// *********************************************************************

class SongListCollection extends GenericCollection {

  function html() {
   
    $html  = "<h3>" . $this->title . "</h3>" . ENDL . ENDL;
    $html .= "<ul>" . ENDL;
    
    foreach ($this as $q => $list)
      $html .= "<li><a href=\"?q=$q\">" . $list->title . "</a></li>\n";
      
    $html .= "</ul>" . ENDL;
    
    return $html;
    
  }

  function sort() { }

}

?>
