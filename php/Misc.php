<?php

function temp() {

  $dat = read("artists.dat");
  $dat["Famille_Denuy_La"]->q = "Famille Denuy, La";
  save($dat, "artists.dat");


}

function artist_ren($old, $neu) {

  // gérer mieux le nom -> t

  foreach ( glob(SONG_DIR."/".$old."__*.dat") as $file ) {

    $tit = substr($file, strpos($file,"__") + 2, -4);

    $old_t = $old . "__" . $tit;  // aka $file
    $neu_t = $neu . "__" . $tit;

  Song::rename($old_t, $neu_t);

  // pas efficace car on ouvre et referme les fichiers (mais arrivera pas souvent)

  }

  global $artists;

  $artists[$neu] = $artists[$old];

  unset($artists[$old]);

  $artists[$neu]->name = $neu;


  foreach($artists[$neu]->disco as $alb) $alb->a = $neu;

  $artists->update_only($neu);

  foreach (array("art", "art_vie") as $ar_name) {

      $ar = read($ar_name . ".dat");
      $ar[$neu] = $ar[$old];
      unset($ar[$old]);
      if ($ar_name != "songs") arsort($ar);
      save($ar,$ar_name . ".dat");

  }

  global $users;

  foreach ($users as $u => $user) {

   // rajouter: vérifier si $neu n'existe pas déjà...
    $users[$u]["art"][$neu] = $users[$u]["art"][$old];
    unset($users[$u]["art"][$old]);

  }

  save($users, "users.dat");

  // *** Reste à déplacer les pochettes ***

}

function delete_artist($a) {

  // fusionner avec artist_ren

  foreach( array( "art", "art_vie", "pond", "pond_vie", "artists" ) as $ar_name ) {

    $filename = $ar_name . ".dat";
    $ar = read($filename);
    unset($ar[$a]);
    save($ar, $filename);

  }

  // manque: voir aussi dans artists
  // boucler sur les delete_songs possibles

  global $users;

  foreach( $users as $user => $dat ) {

    unset($users[$user]["art"][$a]);

  }

  save($users, "users.dat");

  minilog("suppression artiste", $a);

}


function nbtounes() {

  global $songs;

  $res = 0;

  foreach ($songs as $toune) if (!$toune->is_draft()) $res += 1;

  return $res;

}

function daily($f = TRUE) {

  global $artists;

  //
  // Regénérer les fichiers de données
  //

  $tot = 0;
  $cit = 0;
  $nb = 0;
  $drafts = 0;

  // je pense que j'aimerais mieux fonctionner avec un log

  //$songs = new SongCollection("Toute");

  $lost = array();
  $cits = array();

  // on parcourt le dossier des tounes

  foreach (tounes() as $t) {

    $dat = Song::get_metadata($t);

    // pas très clean la gestion des $dat[i]
    if (isset($dat["stats"][1])) {
      $jour[$t] = $dat["stats"][0] - $dat["stats"][1];
      for ($i = 1; $i < 30; $i++)
        if (isset($dat["stats"][$i])) $max = $i;
      $now[$t] = $dat["stats"][0] - $dat["stats"][$max];
    } elseif (isset($dat["stats"][0])) {
      $jour[$t] = $dat["stats"][0];
      $now[$t] = $dat["stats"][0];
    }
    if (isset($dat["stats"][0])) $vie[$t] = $dat["stats"][0];
    if (isset($jour[$t])) $tot += $jour[$t];
    if (!isset($dat["versions"])) echo "$t disparue" . ENDL;
    if (isset($dat["versions"])) {

      $cur = array_key_last($dat["versions"]);
      if ($dat["versions"][$cur]["prog"] == 100) {
       $nb++;
       if (isset($dat["cit"]) && $dat["cit"]) $cit++;
       else $cits[] = $t;
      } else $drafts++;

    }

    if ($f) {
      for ($i = 30; $i > 0; $i--) if (isset($dat["stats"][$i-1])) $dat["stats"][$i] = $dat["stats"][$i-1];
      unset($dat["prev"]);
      unset($dat["next"]);
      Song::set_metadata($t, $dat);
    }

    //$songs[$t] = new Song( $dat );

    if (!isset($dat["versions"])) $lost[] = $t;

  }

  // mise à jour des chansons présentes

  // $artists->update(); nécessaire ??

  // nb de visites par artiste

  $art = array();
  foreach ($now as $t => $n) {
   $aa = substr($t,0,strpos($t, "__"));
   if (!isset($art[$aa])) $art[$aa] = 0;
   $art[$aa] += $n;
  }

  foreach ($art as $t => $dat) {
    if ($artists[$t]->nb > 0) $pond[$t] = $dat / $artists[$t]->nb;
  }

  $artvie = array();
  foreach ($vie as $t => $n) {
   $aa = substr($t,0,strpos($t, "__"));
   if (!isset($artvie[$aa])) $artvie[$aa] = 0;
   $artvie[$aa] += $n;
  }

  foreach ($artvie as $t => $dat) {
    if ($artists[$t]->nb > 0) $pondvie[$t] = $dat / $artists[$t]->nb;
  }

  //arsort($cre);
  arsort($jour);
  arsort($now);
  arsort($vie);
  arsort($art);
  arsort($pond);
  arsort($artvie);
  arsort($pondvie);

  //if ($f) {
   //save($cre, "cre.dat");
   $artists->save();
   //save($songs, "songs.dat");
   save($now, "now.dat");
   save($vie, "vie.dat");
   save($art, "art.dat");
   save($pond, "pond.dat");
   save($artvie, "art_vie.dat");
   save($pondvie, "pond_vie.dat");
  //}

  //
  // Rapport quotidien
  //

  $top  = "Nombre de hits: $tot<br><br>";
  $top .= "Top 15 du jour:<br><ol>";

  for ($i = 0; $i < 15; $i++) {
   $dat = current($jour); $t = key($jour);
   $top .= "<li>" . art($t) . " - " . tit($t) . " ($jour[$t])</li>";
   next($jour);
  }

  $top .= "</ol>";

  $top .= "$nb chansons + $drafts brouillons<br><br>";

  foreach ($lost as $t) {
    Song::restore($t);
    $top .= "* Toune disparue et récupérue * : $t<br><br>";
  }
  
  foreach ($cits as $t) {
    if (!in_array($t, $lost)) $top .= "* À citater * : $t<br><br>";
  }

  $top .= "Adresse IP courante: " . get_IP() . '<br><br>';

  $top .= "Événements du jour:<br><pre>" . minilogOTD() . "</pre>";

  $hdr = "MIME-Version: 1.0\n";
  $hdr .= "Content-type: text/html; charset=\"UTF-8\"\n";
  $hdr .= "From: \"Grimoire du plaisir\" <grimoire.plaisir@gmail.com>\n";
  $hdr .= "Cc: hughes.boulanger@videotron.ca";

  $hier = date("d/m/Y", time() - 60*60*12);
  $yest = date("Y-m-d", time() - 60*60*12);

  if ($f) {
    mail("spongegab@gmail.com", "Rapport du $hier", $top, $hdr);
    save($top, "$yest.html", "data/reports");
  }

  minilog("ok", "rapport quotidien");

  if (date("d") == "01" && $f) rapport(); // Rapport mensuel

  return "<pre>$top</pre>";

}

function cuteDate($old) {

 $mois = Array( 1 => "janvier", 2 => "février", 3 => "mars", 4 =>
         "avril", 5 => "mai", 6 => "juin", 7 => "juillet", 8 =>
         "août", 9 => "septembre", 10 => "octobre", 11 => "novembre",
         12 => "décembre" );

 if (!$old) return "dur à dire";

 if ($old < 1244400000) return "longtemps";

 switch($diff = time() - $old) {
  case $diff < 2*60 :
   return "à l'instant"; break;
  case $diff < 60*60 :
   return "il y a ".floor($diff/60)." minutes"; break;
  case $diff < 2*60*60 :
   return "il y a une heure"; break;
  case $diff < 24*60*60 :
   $h = floor($diff/3600);
   $m = floor($diff/60) - 60*$h;
   return "il y a $h h $m"; break;
  case $diff < 2*24*60*60 :
   return "hier"; break;
  case $diff < 6*24*60*60 :
   $jours = Array("dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi");
   return $jours[date('w',$old)]." dernier"; break;
  case $diff < 604800 :
   return 'il y a une semaine'; break;
  case $diff < 1123200 :
   return "il y a ".floor($diff/86400)." jours";
  break;
  case $diff < 1209600 :
   return 'il y a 2 semaines'; break;
  case $diff < 31449600 :
   return "le ".date('j ',$old).$mois[date("n", $old)];
   break;
  default :
   return $mois[date("n", $old)]." ".date("Y", $old);
 }
}


function barre($prop) {

 $res  = '&nbsp; &nbsp;<span style="width: 150px; border:1px solid black; background: white; display: -moz-inline-box; display: inline-block; vertical-align: middle; text-align: left;">';
 $res .= '<div style="width: '.($prop*150).'px; background: url(media/img/barre.png); text-align: center; font-size: 10pt; color: white;">';
 $res .= number_format(100*$prop, 0).'&nbsp;%</div></span>';

 return $res;

}

function crypto($mot) {  // v 0

 return "v0".bin2hex($mot);

}

function tjrs($yes) {

 return (isset($yes["tjrs"])) ? 2147483647 : 0;

}


function renversi($input) {

  if (($i = strrpos($input, " ")) > 0)
    return substr($input, $i) . ", " . substr($input, 0, $i);
  else
    return $input;

}

function t2fich($t) {

 return "data/$t.txt";

}

function title($q, $w = NULL) {

 global $songs, $artists, $nations, $lists;

 if ($q) {

  if ($w) $suff = "Recherche";
  elseif (strlen($q) == 1) $suff = $q;
  elseif (strpos($q, "__") && file_exists(SONG_DIR."/$q")) {
   $dat = Song::get_metadata($q);
   $suff = "$dat[art] - $dat[tit]";
  } elseif (isset($artists[$q])) $suff = $artists[$q]->name;
  elseif (isset($nations[$q])) $suff = $nations[$q];
  elseif (isset($lists[$q])) $suff = $lists[$q]->title;
  else {

   global $titles;

   $suff = maybe_ar($titles,$q);

  }

 } else $suff = "";
 
 return $suff;

}

function titre($t) {  // obsolète je crois ben

 $fich = file(t2fich($t));
 $titre = rtrim($fich[1]);
 if ($i = strpos($titre, " (1")) $titre = substr($titre, 0, $i);
 if ($i = strpos($titre, " (2")) $titre = substr($titre, 0, $i);
 return $titre;

}

function mod($t) {

 global $mod;
 return $mod[$t];

}

function lien($t, $artist = TRUE) {  // déplacer dans Song

 global $songs;

 if ($i = strpos($t, "__")) {
  $art = substr($t, 0, strpos($t, "__"));
  $brou = (isset($songs[$t]) && $songs[$t]->is_draft()) ? ' class="draft"' : '';
  $lien = "";
  if ($artist)
    $lien .= '<a href="?q='.$art.'" title="'.art($t).'"'.$brou.'>'.art($t).'</a> - ';
  $lien .= '<a href="?q='.$t.'" title="'.art($t)." - ".tit($t).'"'.$brou.'>'.tit($t).'</a>';
  return $lien;
 } else return '<a href="?q='.$t.'" title="'.art($t).'">'.art($t).'</a>';

}

function check($input) {

  return (!strpos(" $input", "<") && !strpos(" $input", ">") && !strpos(" $input", '"'));

}

function uni($input) {

  return stripslashes(iconv("ISO-8859-1", "UTF-8", $input));

}

function compari($a, $b) {

 return ($a["cre"] > $b["cre"]) ? -1 : 1;

}

function acces($dat) {

  return (isset($dat["mod"])) ? $dat["mod"] : $dat["cre"];

}

function tounes() {  // vraiment nécessaire ?

 $dir = opendir(SONG_DIR);
 while ($fich = readdir($dir)) if ($fich != "backup" && $fich != "." && $fich != ".." && $fich != "index.html") 
  $liste[] = $fich;
  
 return $liste;

}

function tit($t) {  // checkée

 global $songs;
 return $songs[$t]->title;

}

function cre($t) {

 global $new;
 return $new[$t];

}

function art($t) {  // checkée

 if (strpos($t, "__")) {
  global $songs;
  return $songs[$t]->artist;
 } else {
  global $artists;
  return $artists[$t]->name;
 }

}

function rapport($vrai = TRUE) {

  $cre = read("cre.dat");
  $mod = read("mod.dat");
  global $annee;

  $mois = date("m", time() - 23 * 60 * 60);
  $year = date("Y", time() - 23 * 60 * 60);

  $debut = mktime(0, 0, 0, $mois, 1, $year);

  $new = array();
  $modif = array();

  foreach ($cre as $t => $tim) {
   if (strtotime($tim) >= $debut) $new[$t] = $tim;
   elseif (isset($mod[$t]) && strtotime($mod[$t]) > $debut) $modif[$t] = $mod[$t];
  }

 if ($new) ksort($new);
 if ($modif) ksort($modif);

 $sjt = "Nouveautés ".$annee[$mois];
 $msg = "<html><body>\n";

 $month = $year . "-" . $mois;

 if (file_exists("html/msg/$month.html")) {
   $repl = array( "GRIM" => URL . "/" );
   $msg .= html_fragment("msg/$month", $repl);
 }

 if ($new) {
  $msg .= "<h4>Nouveautés</h4>\n<ul>\n";
  foreach ($new as $t => $dat) $msg .= " <li><a href=\"".URL."?q=$t\">".art($t)." - ".tit($t)."</a></li>\n";
  $msg .= "</ul>\n\n";
 }

 if ($modif) {
  $msg .= "<h4>Modifications</h4>\n<ul>\n";
  foreach ($modif as $t => $dat) // if (!isset($new[$t]))
   $msg .= " <li><a href=\"".URL."?q=$t\">".art($t)." - ".tit($t)."</a></li>\n";
  $msg .= "</ul>\n\n";
 }

 $end = "<p>Pour ne plus recevoir ce message une fois par mois, répondez-y en disant des choses méchantes.</p>\n";
 $end .= "<p>Keep on rockin'</p>\n<p>&nbsp;&nbsp;Gab pour le <a href=\"".URL."\">Grim</a></p>\n</body>\n</html>";

 $msg .= $end;

 $hdr = "MIME-Version: 1.0\n";
 $hdr .= "Content-type: text/html; charset=utf-8\n";
 $hdr .= "From: \"Grimoire du plaisir\" <grimoire.plaisir@gmail.com>";

 $users = read("users.dat");

 if ($new || $modif) { if ($vrai) {
  foreach ($users as $nom => $dat) if (isset($dat["new"])) mail ($dat["mail"], '=?UTF-8?B?'.base64_encode($sjt).'?=', $msg, $hdr);
 } else return "<h3>$sjt</h3>" . $msg; }

}

function convertart($dat) {

 if (isset($dat[1]) && $dat[1]) {

  if ($dat[1] == "*") return renversi($dat[0]);
  else return $dat[1];

 } else return q_encode($dat[0], TRUE);

}

function decorti($input) {

 $code = substr($input,0,2);

 if ($code == "- " || $code == "* " || $code == "% " || $code == "@ " || $code == "> ") $input = substr($input,2);
 $input = trim(str_replace(array("{","}"), " ", $input));
 return explode("  ", $input);

}

function fich2t($f) {

 $temp = explode("/", $f);
 return substr($temp[1], 0, -4);

}

function horny($mode = ALL_QUOTE) {  // checkée

  global $songs;

  do {
    $q = $songs->random_id();

    $found = !$songs[$q]->is_draft();

    $has_quote = isset($songs[$q]->quote) && $songs[$q]->quote;

    if ($mode == QUOTE)
      $found = $found && $has_quote;
    elseif ($mode == NO_QUOTE)
      $found = $found && !$has_quote;

  } while (!$found);

  return $q;

}

function gugu() {  // checkée

 if (isset($_SERVER["HTTP_REFERER"])) {

  $str = $_SERVER["HTTP_REFERER"];

  # ad hoc

  if ( !strpos($str, "grimoire.pl") && !strpos($str, "192.168.1.99") 
       && !strpos($str, "cache:") && $str) {

   $host = substr($str, 7, strpos($str, "/", 7) - 7);

   $i = strpos($str, "q=");
   $str2 = ($j = strpos($str, "&", $i)) ? substr($str, $i+2, $j-$i-2) : substr($str, $i+2);

   $ii = strpos($str, "p=");
   $str3 = ($j = strpos($str, "&", $ii)) ? substr($str, $ii+2, $j-$ii-2) : substr($str, $ii+2);


   if ($i || $ii) {
    $str = ($i) ? $str2 : $str3;
    minilog(urldecode($str), $host);
   }
   
  }

 }


}
function logguer($t) {

 global $songs;

 if (isset($songs[$t])) {

   $dat = Song::get_metadata($t);
   if (isset($dat["stats"]) && isset($dat["stats"][0])) $dat["stats"][0]++; else $dat["stats"][0] = 1;
   Song::set_metadata($t, $dat);

   $a = substr($t, 0, strpos($t, "__"));

   if (nom()) {

    $users = read("users.dat");
    if (isset($users[nom()]["top"][$t]))
      $users[nom()]["top"][$t]++;
    else
      $users[nom()]["top"][$t] = 1;
    if (isset($users[nom()]["art"][$a]))
      $users[nom()]["art"][$a]++;
    else
      $users[nom()]["art"][$a] = 1;
    $users[nom()]["lastt"] = $t;
    save($users, "users.dat");

  }

  $res = TRUE;
  minilog($songs[$t]->html($t, "minilog"), "toune");

 } else {

  if (valid($t)) minilog($t, "* FUCK * logguer");
  $res = FALSE;

 }

 return $res;

}

function mois($m) { // semble inutilisée

 global $annee;
 $date = explode("-", $m);
 return $annee[$date[1]]." ".$date[0];

}

function col() {

 global $users;
 if (nom() && $users[nom()]["col"]) return $users[nom()]["col"];
 else return 4;

}

// user management

function nom() {

 global $users;

 if ( isset($_POST["login"]) && crypto($_POST["mot"]) == $users[$_POST["nom"]]["mot"] )

  return $_POST["nom"];

 elseif ( isset($_POST["new_user"]) && $_POST["mot"] == $_POST["mot_again"] && trim($_POST["nom"]) && !isset($users[$_POST["nom"]]) )

  return $_POST["nom"];

 elseif ( !isset($_POST["oubli"]) && isset($_COOKIE["mot"]) && $_COOKIE["mot"] == $users[$_COOKIE["nom"]]["mot"] )

  return $_COOKIE["nom"];

 else

  return "";

}

function obsolet($raw) {

 if (isset($raw["q"])) $q = $raw["q"];
 elseif (isset($raw["t"])) $q = $raw["t"];
 elseif (isset($raw["g"])) $q = $raw["g"];
 else $q = "";

 if ($q == "log") $q = "top";
 elseif ($q == "horny") $q = horny();

  if ($_SERVER["SERVER_PORT"] == 8080) {

  header("HTTP/1.1 301 Moved Permanently");
  $lien = ($_SERVER["QUERY_STRING"]) ? "?".$_SERVER["QUERY_STRING"] : "";
  header("Location: ".URL.$lien);
  exit;

 }

 global $artists;

 // redirection

 if (isset($artists[$q]) && $artists[$q]->is_empty() && isset($artists[$q]->ref)) {

   $q = $artists[$q]->ref[0];

 }

 return $q;

}

function ordre() {

 global $users;
 if (nom()) return $users[nom()]["ord"];
 else return "chrono";

}


function messages() {

  $repl = array( "GRIM" => URL . "/" );

  $html = "";

  foreach(glob("html/msg/*") as $filename) {

    $tmp = explode("/", $filename);
    $month = substr($tmp[2],0,-5);
    $html = "<h4>$month</h4>" . html_fragment("msg/$month", $repl) . $html;

  }
  
  $html = "<h3>Archive des messages mensuels</h3>" . $html;

  return $html;

}


function menu($w, $oldq) {

  global $nations, $users, $songs;

  $menu = array();

  // skull

  $menu["SKULL"] = html_fragment("menu/skull");
 
  // perso
 
  $name = nom();
 
  if ($name) {
    
    $repl = array();
    
    $repl["IMG"] = img($name);
    $repl["NAME"] = $name;
    
    $menuitems = array( "Dépotoir" => "dep",
                        "Profil" => "profil",
                        "Suggestions" => "edit" );

    if (isset($users[$name]["menu"])) {
      $menuitems["Éditer menu"] = "menu";
      foreach ($users[$name]["menu"] as $q=>$lien)
        $menuitems[$lien] = $q;
    }
    
    if (superuser()) {
      $menuitems["Brouillons"] = "brouillons";
      $menuitems["Minilog"] = "minilog";
      $menuitems["Rapport"] = "rapport";
      $menuitems["Reports"] = "data/reports";
    }
    
    ksort($menuitems);
    
    $usermenu = "";

    foreach ($menuitems as $item => $q) {
      $usermenu .= '<li><a href="';
      $usermenu .= (strpos($q, "/")) ? "" : '?q=';
      $usermenu .= $q . '" title="' . $item . '">' . $item . '</a></li>' . ENDL;
    }
   
    $repl["USERMENU"] = $usermenu;
    
    // retrier les entrées ? en peuplant d'abord un array
   
    $perso = html_fragment("menu/user", $repl);
 
  } else $perso = html_fragment("menu/perso");
    
  $menu["PERSO"] = $perso;
  
  // alpha
  
  $letters = "";
  
  foreach(array("#", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z") as $lettre) {
    $link = ($lettre == "#") ? "%23" : $lettre;
    $letters .= "  <a href=\"?q=$link\">$lettre</a>" . ENDL;
  }
  
  $menu["ALPHA"] = html_fragment("menu/alpha", array("LETTERS" => $letters));

  // pays
  
  $countries = "";
  
  $j = 0;

  foreach($nations as $code => $pays) { //if ($code != "su") {
  
    if ($j == 0) $countries .= "<tr>";
    $countries .= "<td><a href=\"?q=$code\" title=\"$pays\">";
    if (file_exists("media/pays/$code.png")) $countries .= "<img src=\"media/pays/$code.png\" alt=\"$code\"></a>";
    else $countries .= $code;
    $countries .= "</td>";
    if ($j == 3) { $countries .= "</tr>"; $j = 0; } else $j++;
    
  }
  
  $menu["PAYS"] = html_fragment("menu/pays", array("COUNTRIES" => $countries));
 
  // recherche

  $repl["QQ"] = ($w) ? $oldq : "";
  $repl["SEARCHES"] = "";

  foreach (array("artiste", "disque", "titre", "grep") as $val)
    $repl["SEARCHES"] .= "   <option" . (($w == $val) ? " selected=true" : "") . ">$val</option>" . ENDL;

  $menu["RECH"] = html_fragment("menu/rech", $repl);

  // horny
  
  $menu["HORNY"] = html_fragment("menu/horny", array("HORNY" => horny()));
 
  // add

  if ($oldq) {
  
    $items = '<li><a href="?q=menu&amp;s='.$oldq.'" title="Ajouter au menu">Menu</a></li>';
  
    if (isset($songs[$oldq])) {
      $items  = '<li><a href="?q=dep&amp;s='.$oldq.'" title="Ajouter au dépotoir">Dépotoir</a></li>' . ENDL . $items;
      $items .= '<li><a href="?q=edit&amp;s='.$oldq.'" title="Ajouter aux suggestions">Suggestions</a></li>' . ENDL;
    }

    $menu["ADD"] = ($name && $oldq) ? html_fragment("menu/add", array("ADDITEMS" => $items)) : "";
 
    // edit
  
    if (superuser() && $oldq != "modificatron") {
      $repl["Q"] = "&s=" . urlencode($oldq);
      $menu["EDIT"] = html_fragment("menu/edit", $repl);
    } else $menu["EDIT"] = "";

 
  } else {

    $menu["ADD"] = "";
    $menu["EDIT"] = "";
    
  }

  $repl = Array( "NO" => has_unread_messages(nom()) ? "" : "no_" );
  $menu["CHAT"] = (superuser()) ? html_fragment("menu/chat", $repl) : "";
 
  // all together now
 
  return html_fragment("menu/main", $menu);

}

function clean_dep() {

  global $users;
  global $songs;
   
  foreach ($users as $user => $dat) {
  
    if (isset($dat["dep"])) foreach ($dat["dep"] as $t => $tim) {
    
      if (!isset($songs[$t])) {

        unset($users[$user]["dep"][$t]);
        
      }
      
    }
  
  }
  
  save($users, "users.dat");
  
}


?>
