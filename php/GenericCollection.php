<?php

// *********************************************************************
//
//                         GenericCollection
//
// members:
//
//  str title
//  str filename
//
// methods:
//
//  void __construct(str $title, str $filename)
//  void save()
//  void show() -> children need to implement html()
//  void update() -> children need to implement sort()
//
// *********************************************************************

class GenericCollection extends ArrayObject {

  function __construct($title = NULL, $filename = NULL, $ar = array()) {

    $this->title = $title;
    $this->filename = $filename;
    parent::__construct($ar);

  }

  function save() {

    save($this, $this->filename);

  }

  function update() {

    foreach ($this as $item) $item->update();
    $this->sort();
    if (isset($this->filename)) $this->save();

  }

}

?>
