<?php

// *********************************************************************
//
//                          AlbumCollection
//
// *********************************************************************

class AlbumCollection extends GenericCollection {

  function reverse() {

    $chrono = array();
    $alt = array();

	foreach( $this as $alb ) {

		if (isset($alb->year))
		  $chrono[] = $alb;
		else
		  $alt[] = $alb;

	}

    $chrono = array_reverse( $chrono, true );
    return array_merge($chrono, $alt);

  }

}

?>
