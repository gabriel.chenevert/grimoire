<?php

// *********************************************************************
//
//                          SongCollection
//
// array random_id()
// void add_song($t, $dat)
// static void init_all()
// SongCollection get_drafts()
// void update()
// int nb()
//
// *********************************************************************

class SongCollection extends GenericCollection {

  function random_id() {

    $ar = $this->getArrayCopy();
    return array_rand($ar);

  }

  function add_song($t, $dat) {

    $this[$t] = new Song($dat);
    $this->save();

  }

  static function init_all() {

    $songs = new SongCollection("Toute", "songs.dat");

    foreach (tounes() as $t) {
      $dat = Song::get_metadata($t);
      $songs[$t] = new Song($dat);
    }

    $songs->save();

  }

  function sort() {

    function cmp($a, $b) {
      if ($a == $b) return 0;
      return ($a < $b) ? 1 : -1;
     }

     $this->uasort('cmp');

  }

  function get_drafts() {

   $res = new SongCollection("All drafts", "");

   foreach( $this as $t => $song ) if ($song->is_draft()) {

     $time = $song->cur_time($t);

     $res[$t] = Array( "time" => $time, "prog" => $song->prog($t), "user" => $song->who($t,$time) );
    
    }

   $res->sort();

   return $res;

  }
  
  function nb() {  // counts non-drafts
  
    $nb = 0;
    foreach ($this as $song) if (!$song->is_draft()) $nb += 1;
    return $nb;
  
  }
  
  function nb_in() {
  
    $nb = 0;
    foreach ($this as $song) if (maybe_cl($song, "in")) $nb++;
    return $nb;
    
  }

  function update() {

    foreach ($this as $item) $item->update_status();
    $this->sort();
    if (isset($this->filename)) $this->save();

  }


}

?>
