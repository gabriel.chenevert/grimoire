
<?php

// ******************************************************************************
//
//                                   Song
//
// members:
//
//   str title
//   str artist
//   str realartist
//   str real_title
//   str ar
//   bool draft
//   bool in
//   str quote
//
// methods:
//
//   str ar()
//   bool is_draft()
//   str quote_link()
//   void update_status()
//   str artist()
//   str art_id()
//   str html($li)
//   str top()
//   str show_full()
//   str mmark($type, $i)
//
//   static str cur_time($q)
//   static str get_metadata($q)
//   static str nouv($t,$date)
//   static str set_metadata($q, $dat)
//   static void restore($q)
//   static void rename($old, $neu)
//   static void delete($q)
//   static void replace($old, $neu)
//   static void update($dat)
//
// ******************************************************************************

class Song {

 function __construct($raw = "") {

  // ça commence à être le bordel là-dedans

  //if ($raw["t"]) $this->t = $raw["t"];
  if (isset($raw["tit"])) $this->title = $raw["tit"];
  if (isset($raw["art"]) && $raw["art"]) $this->artist = $raw["art"];
  if (isset($raw["real_title"])) $this->real_title = $raw["real_title"];
  if (isset($raw["ar"])) $this->ar = $raw["ar"];
  if (isset($raw["cit"])) $this->quote = $raw["cit"];
  if (isset($raw["artist"]) && trim($raw["artist"])) $this->artist = $raw["artist"];
  if (isset($raw["real_artist"])) $this->real_artist = $raw["real_artist"];
  if (isset($raw["prog"])) $this->prog = $raw["prog"];

 }

 function ar($def = "") {

  if (isset($this->art)) return q_encode($this->art,TRUE);

  if ($def) return q_encode($def, TRUE);

  $art = ($def) ? $def : $this->art;
  global $artists;

  $tmp = $artists[$this->artist]->q;

  if ($tmp == "*") return renversi($art);
  elseif ($tmp) return $tmp;
  else return q_encode($art, TRUE);

 }
 
  function is_draft() {
    
    if (isset($this->in)) {
      return $this->in == 0;
    }
    else return ($this->prog() < 100);
    
  }
  
  function prog() {

    if (isset($this->prog))
      return $this->prog;
    // else (minilog($this->title . " " . $this->artist, "probreem pas de prog"));
    
  }

  function quotelink($t) {
    
   return '<a href="?q=' . $t . '" title="' . $this->artist . ' - ' . $this->title() . '">' . $this->quote .'</a></div>'; 

  }

 function t($def = "") {

  if (isset($def) && strpos($def, "__")) return $def;

  if (isset($this->realartist)) $art = $this->realartist;
  elseif (isset($this->artist)) $art = $this->artist;
  elseif (isset($this->art)) $art = $this->art;  // obsolet ?
  elseif (isset($def->a)) $art = $def->a;
  else $art = $def;

  $art = $this->art_id($art);

  if (isset($this->real_title)) $title = $this->real_title;
  else $title = $this->title();

  $title = q_encode($title);
  
  return $art . "__" . $title;

 }

 function update_status($def = "") { 

  $t = $this->t($def);

  if (file_exists(SONG_DIR."/".$t)) {
    $this->in = true;

    $cur = $this->cur_version("$t");

    if ($cur["prog"] < 100) {
      $this->draft = true;
    } elseif (isset($this->draft))
      unset($this->draft);
    
  } elseif (isset($this->in))
    unset($this->in);

 }

  function artist() {  // rétrocompatibilité

    if (isset($this->realartist)) return $this->realartist;
    elseif (isset($this->artist)) return $this->artist;
    elseif (isset($this->art)) return $this->art;
    else return "";
  }

  function art_id($def = null) {

    global $artists;

    if (isset($this->real_artist))
      $def = $this->real_artist;
    elseif (isset($this->artist))
      $def = $this->artist;
      
    return $artists->artist_id($def);

  }

function html($album = null, $type="list") {

  global $artists; global $songs;

  $title = $this->title();
  
  if ($type == "minilog") {

    $art = $this->artist;
    $t = $this->t($album);

   return '<a href="' . URL . '/?q=' . $t . '">' . $art . " - " . $title .'</a>';

  }

  $a = isset($album->a) ? $album->a : "";

  $art_id = $this->art_id($a);

  if (isset($artists[$art_id])) {
    $art = (isset($this->real_artist)) ? $this->artist : $artists[$art_id]->name;
    $art = '<a href="' . URL . '?q=' . $art_id . '">' . $art . '</a>';
  } else
    $art = '<span class="sh">' . $this->artist . '</span>';
  $art = ($album) ? "" : "$art - ";

  if ((superuser() && $type != "minilog") || $type == "<<" || $type == ">>") {
   $gab = 'href="?q=modificatron&s=';
   $def = $art_id;
   $gab .= urlencode($def);
   $alb_title = (isset($album->title)) ? urlencode($album->title) . '__' : '';
   $alb_year = (isset($album->year)) ? urlencode($album->year) : '';
   $gab .= '__' . urlencode($title) . '__' . $alb_title . $alb_year .'" title="Ajouter '.$title.'"';
  } else $gab = "";

  $max_size = 20;

  if (strlen($title) > $max_size + 3) $title = iconv_substr($title, 0, $max_size)."&hellip;";  // && $big ?

  $a .= (isset($this->real_artist)) ? $this->real_artist : "";

  $t = $this->t($a);

  $disp_title = (($this->is_draft()) ? "Modifier " : "") . $title;

  $cond = ((isset($this->in) && $this->in) || $type == "minilog") ? 'href="' . URL . '/?q='.$t.'" title="' . $disp_title . '"' : 'class="sh" ' . $gab;
  $brou = (isset($songs[$t]) && $songs[$t]->is_draft()) ? ' class="draft"' : '';

  $txt = ($type == "list" || $type == "minilog") ? $title : $type;

  $tou = '<a ' . $cond . $brou . '>' . $txt .'</a>';

  if ($type == "list")
    return '  <li>' . $art . $tou . "</li>\n";
  elseif ($type == "minilog")
    return $art . $tou;
  else
    return $tou;

 }
 
 
  static function nouv($t,$date) {
    
    $date_format = "short";

    return '  <li>'.lien($t).' <small>'.time_f($date, $date_format)."</small></li>" . ENDL;

  }
  
  
  function top($t, $hits, $pond = FALSE, $art = TRUE) {

    if ($pond) $hits = number_format($hits, 1, ",", "");

    return '  <li>'.lien($t, $art)." <small>$hits</small></li>\n";

  }

  function infobox($q) {

    $dat = Song::get_metadata($q);

    $html = "";

    $html .= '<div id="infobox">';

    $a = q_encode($dat["art"]);
    $alb = q_encode($dat["alb"]);
    $poch = "media/pochettes/" . $a . "__" . $alb . ".jpg";

    if (file_exists($poch)) $html .= "<center><img src=\"$poch\" style=\"width: 115px; height: 115px;\"></center>";

    $html .= '<br><br>';

    if (isset($dat["prev"])) $html .= $dat["prev"]; else $html .= '&nbsp;&nbsp;';

    for ($i = 0; $i < 30; $i++) $html .= "&nbsp;";

    if (isset($dat["next"])) $html .= $dat["next"]; else $html .= '&nbsp;&nbsp;';

    $html .= "<form method=\"post\"><ul>";

    foreach ($dat["versions"] as $i => $hist) {
      $html .= "<li>";
      $html .= "<b>" . str_replace("-", "/", $i) . "</b>";
      if (isset($hist["prog"])) $html .= " " . $hist["prog"] . " %";
      $html .= "<br>";
      if (isset($hist["msg"])) $html .= $hist["msg"];
      $auth = Song::who($q,$i);
      $html .= " <input type=\"submit\" name=\"$i\" value=\"[$auth]\"> ";
      $html .= "</li>";
    }

    $html .= "</ul>";
    $html .= "<input type=\"hidden\" name=\"toggle\"></form>";
    $html .= "<form method=\"post\">";

    $vie = $dat["stats"][0];
    $now = isset($dat["stats"][29]) ? $vie - $dat["stats"][29] : $vie;
    $auj = isset($dat["stats"][1]) ? $vie - $dat["stats"][1] : $vie;
    $html .= "hits ($vie, $now, $auj)";

    $html .= '<br><br>';
    $cit = isset($dat["cit"]) ? $dat["cit"] : "";
    $html .= '<textarea name="cit" id="cit">'.$cit.'</textarea>';
    $html .= '<input type="hidden" name="q" value="'.$q.'"><br><br><input type="submit" value="[cit]">';

    $html .= "</div>";

    return $html;

  }

  static function get_metadata($q) {

    return read("$q/meta.dat");

  }
  
  static function set_metadata($q, $dat) {
    
    save($dat, "$q/meta.dat");
    
  }

  static function cur_version($q) {

    $dat = Song::get_metadata($q);
    if (isset($dat["versions"])) {
     $tim = array_key_last($dat["versions"]);
     return $dat["versions"][$tim];
    } else {
     error("Toune $q disparue");
     return null;
    }

  }

  static function cur_time($q) {

    $dat = Song::get_metadata($q);
    return array_key_last($dat["versions"]);

  }
  
  function init_time($q) {

    $dat = Song::get_metadata($q);
    return array_key_first($dat["versions"]);

  }
  
  function who($q, $ver = Null) {

    $dat = Song::get_metadata($q);
    if (!isset($ver)) $ver = array_key_last($dat["versions"]);
    $res = maybe_ar($dat["versions"][$ver], "qui");

    $conv = array( "" => "?", "hug" => "h", "Gab" => "g", "Grim" => "G" );

    return $conv[$res];
    
  }

  function show_full($q) {

    $html  = "";
    $html .= '<div id="pre"><pre>';

    $dat = Song::get_metadata($q);
    $cur = $this->cur_version($q);

    if (($cur["prog"] < 100) && !superuser()) {

      error("Désolé, cette chanson n'est pas encore prête");

    } else {

      if ($cur["prog"] < 100) $html .= "*** BROUILLON ***" . ENDL . ENDL;

      $a = substr($q,0,strpos($q, "__"));
      $alb = q_encode($dat["alb"]);

      if (superuser()) $html .= $this->infobox($q);

      $html .= "<a href=\"?q=$a\">". ( ($a == "Traditionnel") ? $dat["tit"] : $dat["art"] )."</a>\n";

      $html .= ( ($a == "Traditionnel") ? $dat["art"] : $dat["tit"])."\n";

      if ($alb) { // bricolage
        $tmp = $dat["alb"];
        if ($i = strpos($tmp, " / ")) {
          $tmp = explode(" / ", $tmp);
          $html .= $tmp[0] . " / ";
          $tmp = $tmp[1];
          $alb = q_encode($tmp);
        }
        $html .= "<a href=\"?q=$a#$alb\">$tmp</a>";
      }

      $date = $dat["dat"];
      if ($alb && $date) $html .= " ";
      if ($date) $html .= "($date)";
      if ($alb || $date) $html .= "\n"; $html .= "\n";

      $tim = $this->cur_time($q);

      foreach (explode("\n", read("$q/$tim.txt")) as $i => $ligne) if ($i > 3) {

        $ligne = str_replace(Array("<", ">"), Array("&lt;", "&gt;"), $ligne);

        if (($ii = strpos($ligne, "(goto ")) !== FALSE) {

          $j = strrpos($ligne, ")");
          $tit = substr($ligne, $ii+6, $j - $ii - 6);
          $q = q_encode($tit);
          if ($tit == "I'm Moving On") $a = "Ono_Yoko"; // ad hoc
          $ligne = str_replace("$tit", "<a href=\"?q=$a"."__"."$q\">$tit</a>", $ligne);
        }

        $html .= $ligne;

      }

    }

    $html .= "</pre>" . ENDL . ENDL . "</div>";

    return $html;

  }
  
  function mmark($type = "album", $i = 0) {
    
    switch ($type) {
    
      case "list":
    
        $txt = "$i. " . $this->artist;
        
        if (isset($this->real_artist))
          $txt .= " {" . $this->real_artist . "}";
        
        $txt .= " - " . $this->title();
        
        if (isset($this->real_title))
          $txt .= " {" . $this->real_title ."}";
        
        break;
      
      case "album":
     
        $txt = "- ";
        
        if ($this->artist()) $txt .= "{" . $this->artist() . "} ";
        
        $txt .= $this->title();
        
        if (isset($this->real_title)) $txt .= " {" . $this->real_title ."}";
      
    }
    
    $txt .= ENDL;
    
    return $txt;
    
  }

  function title() {  // retrocompatibility
    
    $res = "";
    
    $res .= (isset($this->title)) ? $this->title : "";
    $res .= (isset($this->tit)) ? $this->tit : "";
    
    return $res;
    
  }


  static function restore($q) {

    $dest   =  SONG_DIR . "/" . $q;
    $backup =  BACKUP_DIR . "/" . $dest . "/meta.dat";

    system("cp $backup $dest", $ok);

    if ($ok == 0)
      minilog("récupération", $q);
    else
      error("problème récupération $q");

  }

  static function rename($old, $neu) {

    print_d("Hello there");

    // ne gère que le déplacement physique + modification des fichiers .dat
    // pas la mise à jour des artistes

    if (file_exists("songs/$neu")) {
      error("erreur: $neu existe déjà");
    } else {

      minilog("déplacement", "$old -> $neu");
      `mv songs/$old songs/$neu`;

      foreach ( array("cre", "mod", "now", "vie", "songs") as $ar_name ) {

        $ar = read($ar_name . ".dat");
        $ar[$neu] = $ar[$old];
        unset($ar[$old]);
        if ($ar_name != "songs") arsort($ar);
        save($ar,$ar_name . ".dat");

      }

      $users = read("users.dat");
      foreach ($users as $n => $user) {
        if (isset($user["top"][$old])) {
          //print_r($users[$n]);
          $users[$n]["top"][$neu] = $users[$n]["top"][$old];
          unset($users[$n]["top"][$old]);
        }

        if ($user["lastt"] == $old) $users[$n]["lastt"] = $neu;

      }

     save($users, "users.dat");

    }

  }


  static function delete($t) {

    Song::replace($t, NULL);

  }

  static function replace($old, $neu) {

    // ne gère que le déplacement physique + modification des fichiers .dat
    // pas la mise à jour des artistes

    $old_f = SONG_DIR."/$old";
    $neu_f = SONG_DIR."/$neu";

    if ($neu)
      if (file_exists($neu_f)) {
        error("$neu existe déjà");
      } else {
        minilog("déplacement", "$old -> $neu");
        rename($old_f, $neu_f);
      }
    else {
      minilog("suppression", $old);
      unlink($old_f . "/meta.dat");
      array_map( "unlink", glob($old_f . "/*.txt") );
      rmdir($old_f);
    }

    foreach ( array("cre", "mod", "now", "vie", "songs") as $ar_name ) {

      $ar = read($ar_name . ".dat");
      if ($neu) $ar[$neu] = $ar[$old];
      unset($ar[$old]);
      if ($ar_name != "songs") arsort($ar);
      save($ar, $ar_name . ".dat");

    }

    $users = read("users.dat");
    foreach ($users as $n => $user) {
      if (isset($user["top"][$old])) {
        if ($neu) $users[$n]["top"][$neu] = $users[$n]["top"][$old];
        unset($users[$n]["top"][$old]);
      }

      if ($user["lastt"] == $old) $users[$n]["lastt"] = $neu;

    }

    save($users, "users.dat");

  }
  
  static function update($post) {

    $cre = read("cre.dat");
    $mod = read("mod.dat");

    global $songs;

    $is_draft = ($post["prog"] < 100);
    $old = $post["q"];

    $ar = convertart(array($post["art"], $post["ar"]));

    if (isset($post["delete"])) {
      Song::delete($old);
      return $ar;
    }

    $ti = ($post["real_title"]) ? $post["real_title"] : q_encode($post["tit"]);
    $t = $ar . "__" . $ti;

    if ($old && $t != $old) Song::replace($old, $t);

    $date = $post["date"];
    $cur = date("Y-m-d", time());
    $was_draft = !isset($cre[$old]);

    if (file_exists("songs/$t"))
      $dat = Song::get_metadata($t);
    else
      mkdir("songs/$t");

    if ($date != $cur && $was_draft && isset($dat)) {
      unset($dat["versions"][$date]);
      $date = $cur;
    }

    $dat["versions"][$date] = Array( "qui" => nom(), "prog" => $post["prog"], "msg" => $post["desc"]);

    if ($is_draft) {

      unset($cre[$t]); save($cre, "cre.dat");
      unset($mod[$t]); save($mod, "mod.dat");

    } else {

      if ($was_draft) {  // nouveauté

        //$dat["stats"][0] = 0; // hits inutile je crois (hits négatifs !)
        save(array($t => $date) + $cre, "cre.dat");

      } elseif ($date != $cre[$old])

        save(array($t => $date) + $mod, "mod.dat");
    }

    $dat["prog"] = $post["prog"];

    if ($is_draft && !$was_draft) {
      unset($cre[$t]); save($cre, "cre.dat");
      unset($mod[$t]); save($mod, "mod.dat");
    }

    foreach(array( "art", "tit", "alb", "dat" ) as $str)
      $dat[$str] = stripslashes($post[$str]);

    Song::set_metadata($t, $dat);

    $txt  = $dat["art"] . ENDL;
    $txt .= $dat["tit"] . ENDL;
    $txt .= $dat["alb"] . " (" . $dat["dat"] . ")" . ENDL . ENDL;
    $txt .= $_POST["txt"];

    if (isset($post["rempl"])) $txt = rempl_alt($txt);

    save($txt, "$t/$date.txt");

    foreach(array( "txt" ) as $str)
      $dat[$str] = $post[$str];

    $songs->add_song($t, $dat);
    save($songs, "songs.dat");

    // $artists->update_only($ar); fait à l'affichage

    message("<a href=\"?q=$t\">$t</a> modificatrée");
    $desc = (isset($min)) ? "mineure" : stripslashes($post["desc"]);
    if ($post["prev"] != $post["prog"])
      $desc .= " (" . $post["prev"] . " % -> " . $dat["prog"] . " %)";
    minilog($songs[$t]->html($t, "minilog"). " -- $desc", (isset($msg)) ? $msg : "modif");

  }


}
