<?php

function main($args) {

  $message = "";

  $q = obsolet($args);

  $w = (isset($args["w"])) ? $args["w"] : NULL;  // recherche
  $s = (isset($args["s"])) ? $args["s"] : NULL;  // action
  $p = (isset($args["p"])) ? $args["p"] : 1;     // page

  if (is_array($tmp = traitement($s, $q, $w))) { $q = $tmp[0]; $s = $tmp[1]; }
  else { $q = $tmp; if ($s == $q) unset($s); }

  $oldq = $q;

  if ($w) {  // recherche

    $liste = rechercher($w, $q);

    if ($liste && count($liste) == 1) {

      if ($w == "grep") {

        foreach ($liste as $qq => $num) $q = $qq;

      } elseif ($w == "titre") {

	$q = $liste[0];

      } else {

        $cur = $liste->getIterator()->current();

        if ($w == "disque") { $q = $cur["art"]; }  // works ?
        else $q = $cur->q();

      }

    unset($liste);

    }

  }

  // display

  $tit = title($q, $w);
  if ($tit) $tit .= " - ";
  $tit .= "Le grimoire du plaisir";

  if (isset($liste)) {

    if ($liste instanceof ArtistCollection) $body = $liste->html();
    else $body = afficher($w, $liste, NULL);

  } else {

    if (!isset($s)) $s = NULL; // pas très propre
    $body = afficher_page($q, $s, $p);

  }

  $menu = menu($w, $oldq);

  global $message;

  $repl = Array( "TIT" => $tit, "MENU" => $menu, "BODY" => $body, "MSG" => $message );

  echo html_fragment("index", $repl);

  
}


function afficher($q, $var1, $var2, $var3 = NULL) {

  $html = "";

  switch ($q) {

    case "disque":

      $html .= "<h3>J'en ai trouvé ".count($var1)."</h3>\n<div class=\"box\">\n <ul>" . ENDL;
      foreach ($var1 as $d => $dat) {
        $html .= "  <li><a href=\"?q=$dat[art]#".q_encode($d)."\">$d</a> <small>$dat[nb]</small><br>" . ENDL; 
        $html .= "  <i><a href=\"?q=$dat[art]\">$dat[nom]</a></i></li>" . ENDL;
       }
       if (count($var1) == 0) $html .= "  Meilleure chance la prochaine fois" . ENDL;
       $html .= " </ul>\n</div>\n\n";

       return $html;

    case "modifications":

      global $mod;

      if (!$var4) $tabl = Array( array_slice($mod, 0, $var2), array_slice($mod, $var2, $var2) );
      else $tabl = array_slice($mod, 0, $var2);

      return afficher("tounes", $tabl, NULL, $var3);

    case "top_now":

      $m = date("m", time());
      global $annee;
      $m = $annee[$m - 1];

      function compi($a,$b) { return ($a["now"] < $b["now"]) ? 1 : -1; }

      uasort ($var1, "compi");
      return afficher("tounes", array_slice($var1, 0, $var2), "now", "<a href=\"?q=top_now\" title=\"Plus de top\">Top de $m</a>");

    case "top_vie":

      function compivie($a,$b) { return ($a["vie"] < $b["vie"]) ? 1 : -1; }
      uasort ($var1, "compivie");
      return afficher("tounes", array_slice($var1, 0, $var2), "vie", "Top global");

    case "brouillons":

      if( ! superuser()) {
        error("Désolé, cette fonctionalité n'est pas accessible aux ploucs");
        break;
      }

      $html .= "<p>Trier plutôt par : ";

      $orders = Array( "last" => "ordre chronologique",
                       "prog" => "progression");

      $s = ($var2) ? $var2 : "last";

      foreach( $orders as $ss => $order ) if ($ss != $s)
        $html .= '<a href="?q=brouillons&amp;s=' . $ss . '">' . $order . '</a> ';
      $html .= "</p>";

      function complast($a,$b) { return ($a["time"] < $b["time"]) ? 1 : -1; }
      function compprog($a,$b) { return ($a["prog"] < $b["prog"]) ? 1 : -1; }
      function comptsal($a,$b) { return ($a["time"] > $b["time"]) ? 1 : -1; }
      function compgorp($a,$b) { return ($a["prog"] > $b["prog"]) ? 1 : -1; }

      $var1->uasort("comp" . $s);

    case "Bgrep":
    case "grep":

      global $songs;

      $liste = $var1;
      $num = count($liste);

      $html .= "<h3>J'en ai trouvé " . $num . "</h3>\n\n<ul style='width: 45%'>"; 

      $i = 0;

      foreach($liste as $t => $n) {
        if ($q == "brouillons") {
          $n = $n["prog"] . " % " . time_f($n["time"]) . " [" . $n["user"] . "]";
        }
        $html .= "<li>".lien($t)." <small>$n</small></li>";
        if ($i++ == floor($num/2)) { $html .= "</ul><ul style='width: 45%'>"; $i = 0; }
      }

      $html .= "</ul>";

      return $html;

    case "titre":
    case "tounes":

      $html .= "<h3>";
      $liste = $var1;
      $html .= ($var3) ? $var3 : "J'en ai trouvé ".count($liste);
      $html .= "</h3>";

      $html .= ($var2) ? "<ol>\n" : "<ul>\n";
 
      foreach ($liste as $t) $html .= "<li>".lien($t)."</li>";

      return $html;

  }

}


function afficher_page($q, $s, $p) {

  global $artists, $songs, $lists, $users;

  $html = "";

  switch($q) {

    case "brouillons":

      return afficher("brouillons", $songs->get_drafts(), $s);

    case "rapport":

      return rapport(FALSE);

    case "messages";

      return messages();

    case "listes":

      return $lists->html();

    case "chatte":

      return read("comparses.dat")->html($s);

    case "edit":

      global $users;

      $sugg = "";

      if ($sugg = maybe_ar($users[nom()],"sugg")) {

        foreach (array_reverse($sugg) as $t => $dat)

          if ($t != "nom" && $t != "time") {

            $sugg .= " <li>\n  <a href=\"?q=$t\">$dat[art] - $dat[tit]</a><br>\n";
            $sugg .= "  <input name=\"$t\" style=\"width: 250px;\" value=\"$dat[comm]\"></input> &nbsp; <label><input type=\"checkbox\"name=\"$t\">del</label><br>\n";
            $sugg .= " </li>\n";

          }

      } else $sugg .= "<i>Aucune suggestion</i>";

      return html_fragment("edit_sugg", Array("SUGG" => $sugg));

    case "feedback":

      $html .= "<h3 class=\"top\">Contacter le maître du web</h3>\n<div class=\"box\">\n";

      if (!nom()) $html .= " <p><i>Afin d'éviter la pollution nocture, pour me dire quelque chose il faut auparavant s'être inscrit (cadenas à gauche). </i></p>\n";

      

      else $html .= html_fragment("feedback", Array("EMAIL" => maybe_ar($_COOKIE, "mail")));

      $html .= "</div>\n";

      return $html;

    case "minilog":

      if (superuser() && file_exists("data/mini.log"))
        $html = '<pre>' . minilogOTD() . "</pre>" . ENDL;

      return $html;

    case "all":

      $html .= "<h3>Toutes les tounes en ordre d'apparition</h3>" . ENDL;
      $html .= '<div class="acc">' . ENDL;

      $cre = read("cre.dat");

      $tab = array_reverse($cre);

      $html .= "<ol>";

      $date_format = "long";

      $i = 0;
      $N = intdiv(count($tab),2);
      foreach ($tab as $t => $date) {
        $html .= "<li>".lien($t)." <small>".time_f($date, $date_format)."</small></li>" . ENDL;
        if ($i++ == $N) $html .= '</ol></div><ol start="' . ($i+1) . '">';
      }

      $html .= "</ol>";

      return $html;

    case "new":

      $html .= "<h3>Nouveautés</h3>" . ENDL;
      $html .= '<div class="acc">' . ENDL;

      $cre = read("cre.dat");

      $tab = array_splice($cre, 0, 2*N());

      $html .= "<ul>";

      $date_format = "short";

      $i = 0;

      foreach ($tab as $t => $date) {
        $html .= Song::nouv($t,$date);
        if ($i++ == N() - 1 && $s != "num") $html .= "</ul></div><ul>";
      }

      $html .= "</ul>";

      return $html;

    case "modif":

      $html .= "<h3>Modifications</h3>\n <div class=\"acc\"><ul>";

      $mod = read("mod.dat");

      $i = 0;

      foreach (array_splice($mod, 0, 2*N()) as $t => $tim) {
        $html .= "<li>".lien($t)." <small>".time_f($tim, "short")."</small></li>";
        if ($i++ == N() - 1) $html .= "</ul></div><ul>";
      }

      $html .= " </ul>";
      return $html;

    case "top":
    case "top_now":
    case "top_vie":

      global $songs;

      $top = ($q == "top_vie") ? read("vie.dat") : read("now.dat");
      $h3 = ($q == "top_vie") ? "Top depuis un boutte" : "Top des 30 derniers jours";

      $html .= "<h3>$h3</h3>";
      $html .= '<div class="acc"><ol>';

      $i = 0;

      foreach (array_splice($top, 0, 2*N()) as $t => $hits) {
        $html .= $songs[$t]->top($t, $hits);
        if ($i++ == N() - 1) $html .= "</ol></div><ol start=".(N() + 1).">";
      }

      $html .= "</ol>";
      return $html;

    case "top_art":
    case "top_art_vie":

      if ($q == "top_art") {
        $html .= '<div class="acc"><h3>Top artistes des 30 derniers jours</h3><div class="acc"><ol>';
        $top = read("art.dat");
        $pond = read("pond.dat");
      } else {
        $html .= '<div class="acc"><h3>Top artistes depuis un boutte</h3><div class="acc"><ol>';
        $top = read("art_vie.dat");
        $pond = read("pond_vie.dat");
      }

      $i = 0;

      foreach (array_splice($top, 0, 2*N()) as $t => $hits) {
        $t = ($t) ? $t : "44"; // bizarre, 44 devient 0
        $html .= $artists[$t]->top($t, $hits);
        if ($i++ == N() - 1) $html .= "</ol></div><div class=\"acc\"><ol start=".(N() + 1).">";
      }

      $html .= "</ol></div></div>";

      $html .= '<div class="acc"><h3>Pondéré par le nombre de chansons</h3><div class="acc"><ol>';

      $i = 0;
      foreach (array_splice($pond, 0, 2*N()) as $t => $hits) {
        $t = ($t) ? $t : "44"; // bizarre, 44 devient 0 !?
        $html .= $artists[$t]->top($t , $hits, TRUE);
        if ($i++ == N() - 1) $html .= "</ol></div><div class=\"acc\"><ol start=".(N() + 1).">";
      }

      $html .= "</ol></div>";

      return $html;

    case "users":

      if (!$s) $s = "last";

      $sort_name = ($s == "nom") ? 's=mon" title="Antitrier par nom' : 's=nom" title="Trier par nom';
      $sort_last = ($s == "last") ? 's=tsal" title="Antitrier par dernière connexion' : 's=last" title="Trier par dernière connexion' ;
      $sort_cre  =  ($s == "cre") ? 's=erc" title="Antitrier par date de création' : 's=cre" title="Trier par date de création';

      $super = (superuser(TRUE)) ? "<td><h4>courriel</h4></td> <td><h4>nouv</h4></td>" : "";

      foreach($users as $nom => $dat) {
        $dat["nom"] = $nom;
        $tabl[strtolower($nom)] = $dat;
      }

      if ($s == "erc") { $tabl = array_reverse($tabl); }

      elseif ($s == "last" || $s == "tsal") {

        function compi($a,$b) {
          $aa = isset($a["last"]) ? $a["last"] : -1;
          $bb = isset($b["last"]) ? $b["last"] : -1;
          return ($aa < $bb) ? 1 : -1;
        }
        uasort ($tabl, "compi");
        $tabl = ($s == "last") ? $tabl : array_reverse($tabl);

      } elseif ($s != "cre") {

        if ($s == "mon") krsort($tabl); else ksort($tabl);

     }

     global $mois;

     $rows = "";

     foreach ($tabl as $nom => $dat) {

       $nom = ($dat["nom"]) ? $dat["nom"] : $nom;
       $rows .= "<tr><td><a href=\"?q=sugg&s=$nom\"><img src=\"media/users/".img($dat["nom"])."\" width=40 height=40 border=0></a></td>";
       $rows .= "<td><a href=\"?q=sugg&s=$nom\">$nom</a></td>";
       $rows .= "<td>".cuteDate($dat["last"] ?? NULL)."</td>";
       $rows .= "<td>".cutedate($dat["cre"] ?? NULL)."</td>";
       if (superuser(TRUE)) {
        $mail = $dat["mail"] ?? NULL;
        $neu  = $dat["new"] ?? NULL;
        $rows .= "<td>$mail</td><td>$neu</td></tr>";
       }
       $rows .= ENDL;

      }

      $repl = Array( "SORT_NAME" => $sort_name, "SORT_LAST" => $sort_last, "SORT_CRE" => $sort_cre, "SUPER" => $super, "ROWS" => $rows );
   
      return html_fragment("users", $repl);

    case "new_user":

      if ($_POST) {

        global $users;
        $nom = trim($_POST["nom"]);

        if (isset($_POST["nom"]) && !$nom) { error("L'utilisateur vide n'est pas le bienvenu ici."); }

        elseif ($users[$nom]) { error("Le nom $nom est déjà pris"); $nom = ""; }

        elseif (!check($nom)) { error("Caractères inacceptables dans le nom<"); minilog ("new_user", "alerte");}

        else error("Il faut écrire le mot pareil les deux fois");

      } else $msg = "Salut et bienvenue dans le monde fantasmagorique du grimoire. Avec un nom d'utilisateur tu pourras faire toutes sortes de choses impossibles. ";

      return html_fragment("new_user", Array( "NAME" => $nom, "MSG" => $msg));

    case "profil":

      $nom = (nom()) ? nom() : $_POST["nom"];
      global $users;
      $dat = $users[$nom];

      if ((isset($_POST["mod"]) || isset($_POST["new"])) && !isset($_POST["mail"]) )
        error("Il faut entrer une adresse de courriel pour recevoir les nouveautés ou les modifications");

      $repl = Array();
      
      $repl["NAME"] = $nom;
      $repl["IMG"] = img();
      $repl["CITY"] = maybe_ar($dat,"ville");
      $repl["DESC"] = maybe_ar($dat,"desc");
      $repl["NEW"] = ($dat["new"]) ? ' checked' : "";
      $repl["MAIL"] = maybe_ar($dat, "mail");
      $repl["CHRONO"] = ($dat["ord"] == "chrono") ? 'checked' : "";
      $repl["NECRO"] = ($dat["ord"] == "necro") ? ' checked' : "";
      
      $items = "";
      
      foreach (array(5,10,15,20,25,40,50,100,"∞") as $n) {

        $items .= "  <label><input type=\"radio\" name=\"N\" value=\"$n\"";
        if ($dat["N"] == $n) $items .= ' checked';
        $items .= ">$n</label>\n";
        
      }
      
      $repl["ITEMS"] = $items;
      
      $larg = "";

      foreach (array(3,4,5,6) as $n) {

        $larg .= "  <label><input type=\"radio\" name=\"col\" value=\"$n\"";
        if (col() == $n) $larg .= ' checked';
        $larg .= ">$n</label>\n";

      }
      
      $repl["LARG"] = $larg;
            
      return html_fragment("profil", $repl);

    case "propos":
    
      $repl = array( "NB_TOUNES" => $songs->nb() );

      return html_fragment("about", $repl);

    case "menu":

      $html .= '<h3>Menu personnel à '.nom().'</h3><form method="post" action="?q=menu"><input type="hidden" name="menu">';

      global $users;

      $menu = $users[nom()]["menu"];
      $html .= '<ul style="float: none;">';
      foreach ($menu as $t => $tit)
        $html .= "<li><input name=\"$t\" value=\"$tit\"> $t <label><input type=\"checkbox\" name=\"del$t\">del</label></li>";
        $html .= "</ul>\n";

      $html .= '<br> &nbsp; &nbsp; <input type="submit" value="Enregistrer"></form>';

      return $html;

    case "dep":

      $html .= '<h3>Dépotoir à '.nom().'</h3><form method="post" action="?q=dep"><input type="hidden" name="dep">';
 
      global $users;

      if ($dep = $users[nom()]["dep"]) {

        $html .= "<ul style=\"float: none;\">\n";
        foreach ($dep as $t => $tim)
          $html .= "<li>".lien($t)." <small>".date("d/m", $tim).'</small> <label><input type="checkbox" name="'.$t."\">del</label></li>\n";
          $html .= "</ul>\n";
          $html .= '<br> &nbsp; &nbsp; <input type="submit" value="Enregistrer"></form>';

      } else $html .= "<i>Aucune toune dans le dépotoir</i>";

      return $html;

    case "sugg":

      $html .= "<p><img src=\"media/users/".img($s)."\" width=40 height=40 border=0 align=right style=\"border: 1px solid black; padding: 8px;\"></p>";
      $html .= "<h3>La page à $s</h3>\n";

      global $users;

      if ($v = $users[$s]["ville"]) $html .= "$v<br><br>";
      if ($d = $users[$s]["desc"]) $html .= "<i>$d</i><br><br>";

      if ($top = $users[$s]["art"]) {

        $html .= '<div class="acc"><h3>Top artistes récent</h3>';

        arsort($top);
        $html .= '<div class="acc"><ol>';

        global $artists;

        $i = 0;

        foreach (array_splice($top, 0, 20) as $t => $hits) {
          $html .= $artists[$t]->top($t,$hits);
          if ($i++ == 9) $html .= "</ol></div><div class=\"acc\"><ol start=11>";
        }

        $html .= "</ol></div></div>";

      }

      if ($top = $users[$s]["top"]) {

        $html .= '<div class="acc"><h3>Top tounes</h3>';

        $top = $users[$s]["top"];
        arsort($top);
        $html .= '<div class="acc"><ol>';

        global $songs;

        $i = 0;
        foreach (array_splice($top, 0, 20) as $t => $hits) {
          $html .= $songs[$t]->top($t, $hits, FALSE, FALSE);
          if ($i++ == 9) $html .= "</ol></div><div class=\"acc\"><ol start=11>";
        }

        $html .="</ol></div></div>\n\n";

        if (nom() == $s) $html .= html_fragment("sugg");

      }

      $html .= "<h4>Suggestions</h4><ul>";

      if (isset($users[$s]["sugg"])) foreach (array_reverse($users[$s]["sugg"]) as $t => $dat) {

        $small = ($dat["time"]) ? " <small>".date("d/m", $dat["time"])."</small>" : "";
        $html .= "  <li><a href=\"?q=$t\">$dat[art] - $dat[tit]</a>$small<br>\n  <i>$dat[comm]</i></li>\n";

      } else $html .= "<i>Aucune suggestion</i>";

      $html .= "</ul>";

      return $html;

    case "test":

      if (superuser(TRUE)) temp();
      break;

    case "modificatron":

      if (superuser() && (strpos($s, "__") || !$s)) { // toune

        // todo: mettre une bonne partie dans Song

        $tmp = explode("__", stripslashes($s));
        if (file_exists(SONG_DIR."/$s")) {
          $q = $s;
          $dat = Song::get_metadata($q);
          $cur = array_key_last($dat["versions"]);
          $prog = $dat["versions"][$cur]["prog"];
          $body = read("$q/$cur.txt");

          if ((time() - strtotime($cur)) < 60*60*24*5) {  // recent (5 days)
            $desc = $dat["versions"][$cur]["msg"];
            $who = (isset($dat["versions"][$cur]["qui"])) ? $dat["versions"][$cur]["qui"] : "Gab";
            $date = $cur;
          } else {
            $desc = "modification";
            $who = nom();
            $date = date("Y-m-d", time());
          }
        }
        else {
          $q = "";
          $who = nom();
          $date = date("Y-m-d", time());
          $prog = 0;
          $body = "";
          $desc = "brouillon";
        }

        global $artists;

        $repl = Array();

        $repl["ACTION"] = $tmp[0];
        $repl["Q"] = $q;
        $repl["ART"] = htmlspecialchars(!($q) ? $artists[$tmp[0]]->name : art($q));
        $repl["AR"] = $artists[$tmp[0]]->q();
        $repl["TIT"] = (!$q) ? $tmp[1] : tit($q);
        $repl["REAL_TIT"] = substr($q, strpos($q, "__") + 2);
        $repl["ALB"] = htmlspecialchars((!$q) ? $tmp[2] : $dat["alb"]);
        $repl["DAT"] = (isset($tmp[3])) ? $tmp[3] : ((isset($dat["dat"])) ? $dat["dat"] : "");
        $repl["DESC"] = $desc;
        $repl["WHO"] = $who;
        $repl["PROG"] = $prog;
        $repl["PREV"] = $prog;
        $repl["DATE"] = $date;
        $repl["REMPL"] = ($prog < 80) ? "checked" : "";

        $chars = "";
        foreach(array("♯", "♭", "𝄪", "𝄫", "♮", "°", "~", "‡", "br", "<", ">", "[", "]", "{", "}", "«", "»", "br", "(reste)", "Tune down", "br", "Refrain", "(fade out)", "br", "1,2,3,4", "(embarque)", "br", "w/ solo", "œ", "x 2") as $c) {
          if ($c == "br") $chars .= "<br><br> ";
          else $chars .= '<span onclick=\'javascript:insert("' . $c . '")\'>' . $c . '</span> ';
        }

        $repl["CHARS"] = $chars;
        $repl["TXT"] = "";

        foreach (explode("\n", $body) as $i => $line) if ($i > 3) $repl["TXT"] .= $line;

        return html_fragment("edit_song", $repl);

      } elseif (superuser()) {  // edit

        if (strlen($s) == 1) return html_fragment("add_artist");

        elseif (isset($lists[$s])) return $lists[$s]->edit($s);

        elseif (isset($artists[$s])) return $artists[$s]->edit();

      }

    default:

      global $nations;

      $html = "";

      if (isset($nations[$q])) {  // pays

        $res = $artists->ofcountry($q);

         print_d(count($res));

        return $res->html($nations[$q]." &nbsp; <img src=\"media/pays/$q.png\">");

      } elseif ($q && strpos($q, "__") && $s != $q) {  // toune

        if (!logguer($q)) error("FUCK: " . htmlspecialchars($q));

        else return $songs[$q]->show_full($q);

      } elseif ($q && strlen($q) == 1) {  // lettre

        $res = $artists->startswith($q);
        return $res->html("Artistes commençant par " . $q);

      } elseif ($q && isset($artists[$q])) { // artiste

        $artists->update_only($q);
        return $artists[$q]->html();

      } elseif (isset($q) && isset($lists[$q])) { // liste

        return $lists[$q]->html($q, $p);

      } elseif ($q == "pochette") { // pochette

        $repl["A"] = substr($s, 0, strpos($s, "__"));

        $repl["POCH"] = (file_exists("media/pochettes/$s.jpg")) ? " <center><img src=\"media/pochettes/$s.jpg\"></center>" : " n'a pas";
       
        $repl["S"] = $s;
       
        return html_fragment("poch", $repl);

      } elseif ($q == "stats") {
   
        $repl = array();
        
        // nouveautés
        $cre = read("cre.dat"); $cur = "";
        foreach (array_slice($cre,0,5) as $t => $tim) $cur .= Song::nouv($t,$tim);
        $repl["NEW"] = $cur;
        
        // modifications
        $mod = read("mod.dat"); $cur = "";
        foreach (array_slice($mod,0,5) as $t => $tim) $cur .= Song::nouv($t, $tim);
        $repl["MOD"] = $cur;

        // top now
        $now = read("now.dat"); $cur = "";
        foreach(array_slice($now, 0, 5) as $t => $hits) $cur .= $songs[$t]->top($t, $hits);
        $repl["NOW"] = $cur;

        // top vie
        $vie = read("vie.dat"); $cur = "";
        foreach(array_slice($vie, 0, 5) as $t => $hits) $cur .= $songs[$t]->top($t, $hits);
        $repl["ACC"] = $cur;

        // top art
        $art = read("art.dat"); $cur = "";
        foreach(array_slice($art, 0, 5) as $t => $hits) $cur .= $artists[$t]->top($t, $hits);
        $repl["ART"] = $cur;

        $art = read("pond.dat"); $cur = "";
        foreach(array_slice($art, 0, 5) as $t => $hits) $cur .= $artists[$t]->top($t, $hits, TRUE);
        $repl["POND"] = $cur;

        // top art vie
        $art = read("art_vie.dat"); $cur = "";
        foreach(array_slice($art, 0, 5) as $t => $hits) $cur .= $artists[$t]->top($t, $hits);
        $repl["ARTVIE"] = $cur;

        $art = read("pond_vie.dat"); $cur = "";
        foreach(array_slice($art, 0, 5) as $t => $hits) $cur .= $artists[$t]->top($t, $hits, TRUE);
        $repl["PONDVIE"] = $cur;

        $repl["NBSONGS"] = $songs->nb();
        $repl["NBDRAFTS"] = count($songs->get_drafts());

        return html_fragment("stats", $repl);

      } elseif ($q == "home" || !$q) {

        $repl = Array();

        // citation
        $t = horny(QUOTE);
        $repl["CIT"] = $songs[$t]->quotelink($t);

        // nouveautés
        $cre = read("cre.dat"); $cur = "";
        foreach (array_slice($cre,0,25) as $t => $tim)
          $cur .= $songs[$t]->nouv($t, $tim);
        $repl["NEW"] = $cur;

	// logo grim
        $today = date("m-d");
        $tmp = in_array($today, array("06-23", "06-24","07-01","07-14")) ? $today : "default";
	$repl["GRIM"] = "media/grim/" . $tmp . ".png";

        return html_fragment("home", $repl);

      } else {  // accueil

        if ($q) {
        error("$q ça l'existe pas");
        if (valid($q)) minilog($q, "* FUCK * ça l'existe pas");

      }

    }

  }

}


function rechercher($w, $q) {

 global $artists; global $songs;
 minilog ($q, $w);
 $res = array();

 switch ($w) {

  case "artiste":

   $res = $artists->contains($q);

   break;

  case "disque":

   $pelote = explode(" ", str_replace(",", " ", $q));

   foreach ($artists as $a => $art) foreach ($art->disco as $dat)

   if (isset($dat->title)) {

     $found = 1;
     $d = $dat->title;
     $foin = " ".strtolower(accents($d));

     foreach ($pelote as $aig) if ($aig) {
       $chiche = $found && strpos($foin, strtolower(accents($aig)));
       $found = $chiche;
     }

     if ($found) $res[$d] = array( "art" => $a, "nom" => $art->name, "title" => $d, "nb" => count($dat) - 1);

   }

   break;

  case "grep":

   global $songs;

   $needle = stripslashes($q);
   $res = new SongCollection();

   foreach ($songs as $t => $dat) {

    $ddat = Song::get_metadata($t);

    $tim = array_key_last($ddat["versions"]);

    $where = read("$t/$tim.txt");

    $i = -1;

    while ($i = strpos(" ".$where, $needle, $i+1))

      increment($res,$t);

   }

   $res->sort();

   break;

  case "titre":

   $pelote = explode(" ", str_replace(",", " ", $q));

   foreach ($songs as $t => $dat) {
    $found = 1;

    foreach ($pelote as $aig) if ($aig) $found = $found && strpos(" ".strtolower(accents($dat->title)), strtolower(accents($aig)));
    if ($found) $res[] = $t;

   }

   break;

 }

 return $res;

}


function traitement($s = NULL, $q = NULL, $w = NULL) {

  global $songs, $users, $artists, $lists;
  $q = stripslashes($q);

  if (isset($_POST["suicide"])) {  // Suppression d'un utilisateur

    minilog(nom(), "suicide");
    error(nom()." n'est plus");
    unset($users[nom()]);
    save($users, "users.dat");

  } elseif (isset($_POST["new_user"])) {  // Nouvel utilisateur

    $nom = $_POST["nom"];

    if (check($nom) && !$users[$nom] && $_POST["mot"] == $_POST["mot_again"]) {

      $users[$nom]["mot"] = crypto($_POST["mot"]);
      $users[$nom]["cre"] = time();
      $users[$nom]["img"] = "default.jpg";
      save($users, "users.dat");
      message("Utilisateur $nom créé");
      minilog($nom, "nouvel utilisateur");
      return $q;

    } else return "new_user";

  } elseif (isset($_POST["login"])) {  // Identification

    $nom = $_POST["nom"];

  if ($users[$nom]["mot"] == crypto($_POST["mot"])) {
    setcookie("nom", $_POST["nom"], tjrs($_POST));
    setcookie("mot", crypto($_POST["mot"]), tjrs($_POST));
    minilog ($nom, "login");
  } elseif ($users[$nom]) error("Mot de passe incorrect");
  else error("Utilisateur inexistant");

  return $q;

 } elseif (isset($_POST["oubli"])) {

  setcookie("nom", "", time() - 1000);
  setcookie("mot", "", time() - 1000);
  minilog (maybe_ar($_COOKIE,"nom"), "logout");

  return "";  // *** à arranger ***

} elseif (isset($_POST["admin"]) && superuser()) {

  switch ($_POST["admin"]) {

    case "list":  // liste

      $lists[$q] = new SongList($_POST["list"]);
      $lists->update();
      minilog($q, "modif liste (alpha)");

      return $_POST["q"];

   case "new artist":  // nouvel artiste

    $art = new Artist();
    $art->name = $_POST["name"];
    if ($_POST["q"]) $art->q = $_POST["q"];

    if (isset($artists[$art->q()]))
      error( "artiste " . $art->q() . " semble déjà exister" );
    else {
      $artists->set_artist($art->q(), $art);
      minilog($art->name, "nouvel artiste");
    }

    return Array("modificatron", $art->q());

   case "artiste":  // artiste

    $art = new Artist($_POST);
    $artists->set_artist($art->q(), $art);
    
    message($art->name . " modifié");
    minilog(stripslashes($art->name), "master");
    
    return $q;

   case "modificatron": // modificatron

    Song::update($_POST);

    $ar = convertart(array($_POST["art"], $_POST["ar"]));

    return $ar;

  }

 } elseif (isset($_POST["msg"])) {  // courriel

  $mail = $_POST["mail"];
  setcookie("courriel", $mail, 2147483647);
  $nom = nom();
  if (strpos($nom, " ") || strpos($nom, "(")) $nom = "\"$nom\"";
  $msg = stripslashes($_POST["msg"]);

  if (mail ("spongegab@gmail.com", "Feedback grimoire", $msg, "From: Grimoire du plaisir <grimoire.plaisir@gmail.com>\nReply-To: $nom <$mail>", "-f $mail"))
  //if (mail ("spongegab@gmail.com", "Feedback grimoire", $msg))
   message("Ti-message envoyé");
  else
   error("Ça l'a pas marché");

 } elseif (isset($_POST["menu"])) {  // menu

  foreach ($users[nom()]["menu"] as $t => $tit) {
   $users[nom()]["menu"][$t] = $_POST[$t];
   if (isset($_POST["del$t"])) unset ($users[nom()]["menu"][$t]);
  }
  save($users, "users.dat");
  minilog ("edit", "menu");


 } elseif (isset($_POST["dep"])) {  // dépotoir

  foreach ($_POST as $i => $on) if ($on) unset($users[nom()]["dep"][$i]);
  save($users, "users.dat");
  minilog ("edit", "dépotoir");
  return "dep";


 } elseif (isset($_FILES["poch"]) && superuser()) {  // pochette

  $alb = $_POST["alb"];

  if (!$_FILES["poch"]["tmp_name"]) error("$alb : Fichier trop volumineux");

  else {

    if (file_exists("media/pochettes/$alb.jpg")) unlink("media/pochettes/$alb.jpg");
    move_uploaded_file($_FILES["poch"]["tmp_name"], "media/pochettes/$alb.jpg");
    `mogrify media/pochettes/$alb.jpg -resize 300x300! media/pochettes/$alb.jpg`;
    message("Pochette $alb: ok");
    minilog($alb, "pochette");

  }

  return $q;

 } elseif (isset($_POST["profil"])) {  // profil

  if ( ($_POST["new"] || $_POST["mod"]) && !$_POST["mail"] ) return "profil";

  $nom = $_POST["nom"];
  minilog("$nom", "profil");

  $suff = substr($_FILES["img"]["name"], -3);
  $old = $users[$nom]["img"];

  if ($suff) {
   if ($old) unlink("media/users/$nom.$old");
   move_uploaded_file($_FILES["img"]["tmp_name"], "media/users/".accents($nom).".$suff");
   $users[$nom]["img"] = $suff;
  }

  if (isset($_POST["default"])) {
   unlink("media/users/$nom.$old");
   unset($users[$nom]["img"]);
  }

  $users[$nom]["ville"] = $_POST["ville"];
  $users[$nom]["desc"] = $_POST["desc"];
  $users[$nom]["new"] = $_POST["new"];
  // $users[$nom]["mod"] = $_POST["mod"];
  $users[$nom]["mail"] = $_POST["mail"];
  $users[$nom]["ord"] = $_POST["ord"];
  $users[$nom]["N"] = $_POST["N"];
  $users[$nom]["col"] = $_POST["col"];

  save($users, "users.dat");
  message("Profil enregistré");

  return "";

 } elseif (isset($_POST["vider"])) {  // vider l'historique

  unset($users[nom()]["art"]);
  unset($users[nom()]["top"]);
  message("Historique effacé");
  save($users, "users.dat");

  return "sugg";

 } elseif (isset($_POST["sugg"])) {  // favoris

  $nom = nom();

  foreach ($_POST as $t => $comm) {
   if ($t != "sugg") $users[$nom]["sugg"][$t]["comm"] = $comm;
   if ($comm == "on") unset($users[$nom]["sugg"][$t]);
  }

  if (count($users[$nom]["sugg"]) < 1) unset($users[$nom]["sugg"]);

  minilog("edit", "sugg");

  return save($users, "users.dat");

 } elseif ($s && $q == "menu") {

  $users[nom()]["menu"][$s] = title($s);
  save($users, "users.dat");
  minilog($s, "menu");
  message("$s ajoutée au menu");
  return $s;

 } elseif ($s && $q == "dep") {  // dépotoir

  $users[nom()]["dep"][$s] = time();
  save($users, "users.dat");
  minilog($s, "dépotoir");
  message("$s ajoutée au dépotoir");
  return $s;

 } elseif ($s && $q == "edit") {  // ajouter une suggestion

  $id = nom(); $title = $songs[$s]["title"]; $art = $songs[$s]["art"];

  $users[$id]["sugg"][$s] = Array( "comm" => "Crès belle chanson", "time" 
=> time(), "title" => $title, "art" => $art );

//  $sugg = array_merge(array($id => $temp), $sugg);

  if ($id) {
   minilog ("add $art - $tit", "sugg");
   save($users, "users.dat");
   return "edit";
  }

 } elseif (isset($_POST["chatte"])) {  // Boîte à comparses

  $chat = read("comparses.dat");
  
  $n = count($chat);
  
  $init = $_POST["reply-to"];
  
  if (!isset($chat[$init]["replies"])) $chat[$init]["replies"] = array();
  
  $chat[$init]["replies"][] = $n;
  
  $chat->bump($init);
  
  $chat[$n] = array( "id" => nom(), "time" => time(), "msg" => $_POST["chatte"]);
  
  $f = $_FILES["fich"]["name"];
  
  if ($f) {

    move_uploaded_file($_FILES["fich"]["tmp_name"], "media/chat/" .$f);
    
    $chat[$n]["fich"] = $f;
    
    message("Fichier $f chargé");

  }
  
  $other = (nom() == "Gab") ? "hug" : "Gab";
  set_unread_messages($other, $n);
  
  $chat->save();

  return $q;

 } elseif (isset($_POST["toggle"])) {  // paternité

  $i = key($_POST);
  $tou = Song::get_metadata($q);
  if(isset($tou["versions"][$i]["qui"]) && $tou["versions"][$i]["qui"] == "hug") $tou["versions"][$i]["qui"] = "Gab";
  else $tou["versions"][$i]["qui"] = "hug";
  Song::set_metadata($q, $tou);
  minilog("$tou[art] - $tou[tit] ($i)", "paternité");
  return $q;

 } elseif (isset($_POST["cit"])) {  // citation

  $t = $_POST["q"];
  $dat = Song::get_metadata($t);
  $dat["cit"] = htmlspecialchars($_POST["cit"]);
  Song::set_metadata($t, $dat);
  $songs->add_song($t, $dat);

  minilog($songs[$t]->html($t, "minilog"), "cit");

  return $q;

 } elseif (!strpos($q, "__") && !$w && strlen($q) > 1 && ord($q[0]) < 90) { // artiste

  if (isset($artists[$q]) || isset($lists[$q])) return $q;
  elseif (superuser()) return Array( "modificatron", $q[0] );

 } else return $q;

}


?>
