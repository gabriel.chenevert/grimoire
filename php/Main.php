<?php

error_reporting(E_ALL & ~E_NOTICE);
ini_set("display_errors", 1);
ini_set("default_charset", "UTF-8");

date_default_timezone_set("Europe/Paris");

// constants

define('DOMAIN', "grimoire.pl");
define('URL', "https://" . DOMAIN);
define("QUOTE", 0);
define("NO_QUOTE", 1);
define("ALL_QUOTE", 2);
define("ENDL", "\n");
define("SPC", "&nbsp;");
define("BACKUP_DIR", "backup/current");
define("SONG_DIR", "songs");

// classes

require "Artist.php";
require "Song.php";
require "GenericCollection.php";
  require "ArtistCollection.php";
  require "Chat.php";
  require "SongCollection.php";
    require "Album.php";
    require "SongList.php";
 require "AlbumCollection.php";
 require "SongListCollection.php";

// functions

require "Logic.php";
require "Misc.php";
require "Tools.php";
require "Users.php";

// global data structures

$songs   = read("songs.dat");        // SongCollection
$artists = read("artists.dat");      // ArtistCollection
$lists   = read("lists.dat");        // SongListCollection
$users   = read("users.dat");

// tables

$annee = Array(
  "01" => "de janvier",
  "02" => "de février",
  "03" => "de mars",
  "04" => "d'avril",
  "05" => "de mai",
  "06" => "de juin",
  "07" => "de juillet",
  "08" => "d'août",
  "09" => "de septembre",
  "10" => "d'octobre",
  "11" => "de novembre",
  "12" => "de décembre"
);

$nations = Array(
  "us" => "États-Unis",   // 536 (2023/01/19)
  "fr" => "France",       // 255
  "qc" => "Québec",       // 250
  "uk" => "Royaume-Uni",  // 195
  "ca" => "Canada",       // 72
  "tv" => "Télévision",   // 31
  "au" => "Australie",    // 22
  "se" => "Suède",        // 20
  "be" => "Belgique",     // 19
  "it" => "Italie",       // 10
  "ie" => "Irlande",      // 9
  "su" => "United Shit",
  "cf" => "Rednecks",     // 8
  "nl" => "Pays-Bas",
  "ac" => "Acadie",       // 7
  "jm" => "Jamaïque",
  "sc" => "Écosse",
  "de" => "Allemagne",    // 6
  "dk" => "Danemark",     // 4
  "ch" => "Suisse",       // 3
  "es" => "Espagne",
  "fi" => "Finlande",
  "ro" => "Roumanie",
  "br" => "Brésil",       // 2
  "co" => "Colombie",
  "dz" => "Algérie",
  "gr" => "Grèce",
  "jp" => "Japon",
  "mx" => "Mexique",
  "pr" => "Puerto Rico",
  "ar" => "Argentine",    // 1
  "at" => "Autriche",
  "bb" => "Barbades",
  "ci" => "Côte d'Ivoire",
  "is" => "Islande",
  "in" => "Premières Nations",
  "kr" => "Corée du Sud",
  "ml" => "Mali",
  "mn" => "Mongolie",
  "ng" => "Nigeria",
  "no" => "Norvège",
  "pl" => "Pologne",
  "ru" => "Russie",
  "tt" => "Trinité-et-Tobago",
  "ur" => "Urssie",
  "za" => "Afrique du Sud",
  "xx" => "Non défini"     // 0
);

$titles = array(
  "all" => "Toute",
  "brouillons" => "Brouillons",
  "chatte" => "Boîte à comparses",
  "dep" => "Dépotoir",
  "edit" => "Suggestions à ".nom(),
  "feedback" => "Feedback",
  "home" => "Accueil",
  "listes" => "Listes",
  "menu" => "Menu à ".nom(),
  "messages" => "Messages mensuels",
  "minilog" => "Minilog",
  "modif" => "Modifications",
  "modificatron" => "Modificatron",
  "new" => "Nouveautés",
  "pochette" => "Pochette",
  "profil" => "Profil de ".nom(),
  "propos" => "À propos",
  "rapport" => "Rapport mensuel",
  "stats" => "Statistiques",
  "sugg" => "Suggestions",
  "test" => "Test / temp",
  "top" => "Top actuel",
  "top_vie" => "Top global",
  "top_art" => "Top artistes actuel",
  "top_art_vie" => "Top artistes global",
  "users" => "Utilisateurs"
);

?>
