<?php

// ********************************************************************
//
//                             Artist
//
// members:
//
//   str q
//   str name
//   str nat
//   str sortname
//   array ref
//   array href
//   AlbumCollection disco  // artist *is* or *has* albumcollection ??
//   int nb
//   int tot
//
// methods:
//
//   void __construct( str or array $raw )
//   bool big()
//   void edit()
//   void update()
//   str sortname()
//   str mmark()
//   void html()
//
//   static void delete()
//
// ********************************************************************

class Artist {

  function __construct($raw = NULL) {

    if (is_object($raw) && get_class($raw)== "Artist") {

      error("constructeur de recopie non implémenté");
      $raw = NULL;

    } elseif (is_array($raw)) {

      $this->name = $raw["name"];
      $this->nat = $raw["nat"];
      if (isset($raw["q"]) && $raw["q"]) $this->q = $raw["q"];
      $raw = $raw["disco"];

    } elseif (!$raw) {

      $this->name = NULL;
      $this->nat  = NULL;

    }

    // mettre ailleurs peut-être ?

    $this->disco = new AlbumCollection();
    
    foreach (explode("\n", stripslashes($raw ?? "")) as $ligne) if ($ligne) {

      $mode = substr($ligne, 0, 2);
      $d_ligne = decorti($ligne);

      switch ($mode) {

        case "\r": case "\n": case "\r\n": case "\n\r":

          if (isset($album) && !$debut) {
            $this->disco[] = $album;
            unset($album);
          }
          break;

        case "> ":  // voir aussi

         $aa = convertart($d_ligne);
         $this->ref[] = $aa;
         break;

        case "@ ":  // lien web

         $this->href[$d_ligne[0]] = $d_ligne[1];
         break;

        case "* ":  // album

          if (isset($album)) $this->disco[] = $album;
          $album = new Album( $this->q(), $d_ligne[0] );
          if (isset($d_ligne[1])) $album->year = $d_ligne[1];
          $debut = true;
          break;

        case "- ":  // toune

         if (!isset($album)) $album = new Album( $this->q() );

         $pos = 0; $brut = [];

         // fragile
         if ($ligne[2] == "{") { $brut["art"] = $d_ligne[0]; $pos++; }
         
         $brut["tit"] = $d_ligne[$pos];

         if (count($d_ligne) > $pos+1 && trim($d_ligne[$pos+1])) $brut["real_title"] = $d_ligne[$pos+1];

         $album[] = new Song($brut);

         $debut = false;

      }

    }

    if ((count($this->disco) == 0) && !isset($album)) $album = new Album( $this->q() );

    if (isset($album)) $this->disco[] = $album;

  }


  function sortname() {

    if (isset($this->sortname)) {

     $res = $this->sortname;

    } else {

      $res = prepo($this->name);

      if (isset($this->q)) {
        if ($this->q == "*") $res = renversi($res);
          else $res = $this->q;
      }

    }

    return $res;

  }

  function q() {

    return q_encode( $this->sortname() );

  }

  function big() {

    return (count($this->disco) > 1 || isset($this->disco[0]->title)); 

  }

  function html() {

    global $nations;
    $nompays = $nations[$this->nat];
    $drap = '&nbsp; <a href="?q=' . $this->nat . '"><img src="media/pays/' . $this->nat . '.png" border=none style="vertical-align: middle;" title="' . $nompays . '"></a>';

    if ($this->tot) $prop = barre($this->nb/$this->tot);

    $html = "";

    if (superuser()) {
     $mod1 = "<a href=\"?q=modificatron&s=" . $this->q() . "\" title=\"Modifier $this->name\">";
     $mod2 = "</a>";
    } else {
     $mod1 = "";
     $mod2 = "";
    }

    if ($this->big()) $html .= "<h2>$mod1$this->name$mod2 $drap$prop</h2>\n\n";
    else $html .= "<h3>$mod1$this->name$mod2 $drap</h3>";

    if (isset($this->href)) {

     $html .= "<p>Hors d'ici: ";
     $prem = TRUE;
     foreach($this->href as $tit => $url) {
      if (!$prem) $html .= ", ";
      $html .= "<a href=\"$url\">$tit</a>";
      $prem = FALSE;
     }
     $html .= "</p>";

    }

    if (isset($this->ref)) {

     $html .= "<p>Voir " . (($this->is_empty()) ? "plutôt" : "aussi") . " : ";
     $prem = TRUE;
     global $artists;

     foreach($this->ref as $a) {
      if (!$prem) $html .= ", ";
      $name = $artists[$a]->name;
      $html .= "<a href=\"?q=$a\">$name</a>";
      $prem = FALSE;
     }

     $html .= "</p>";

    }

    if ($this->big()) {

     $html .= "<table id=\"disco\">\n\n";
     $i = 0;

     $disco = (necro()) ? $this->disco->reverse() : $this->disco;

     foreach ($disco as $album) {
      if ($i == 0) $html .= "<tr>";
      $html .= "<td>\n";
      $html .= $album->html();
      $html .= "</td>\n";
      $i++; if ($i == col()) { $html .= "</tr>"; $i = 0; }
     }

     $html .= "</tr></table>";

    } else $html .= $this->disco[0]->html();

    return $html;

  }

  function edit() {

    global $nations;

    $select = "";
 
    foreach ($nations as $code => $pays) {
     $select .= '<option value="'.$code.'"';
     if ($this->nat == $code) $select .= " selected=\"selected\"";
     $select .= ">$pays</option>\n";
    }

    $repl = Array(
      "Q" => (isset($this->q)) ? $this->q : "",
      "QQ" => $this->q(),
      "NAME" => $this->name,
      "NATIONS" => $select,
      "MMARK" => $this->mmark(false)
    );

    return html_fragment("edit_artist", $repl);

  }


  function update() {

    $nb = 0; $tot = 0;

    $a = $this->q();

    $cur = NULL;

    foreach ($this->disco as $album) {

      foreach ($album as $n => $song) {

        $song->update_status($this->q());
        if (isset($song->in) && $song->in && !(isset($song->draft) && $song->draft)) $nb++;
        $tot++;

        $prev = $cur;
        $cur = $song;

        // éviter de ré-écrire chaque fichier jusqu'à 3 x ?
        // ex. tout faire en mémoire et écrire à la fin...

        if (isset($prev->in) && $prev->in && $cur) {
          $t = $prev->t($a);
          $dat = Song::get_metadata($t);
          $dat["next"] = $cur->html($album, ">>");
          Song::set_metadata($t, $dat);
        }

        if (isset($cur->in) && $cur->in && $prev) {
          $t = $cur->t($a);
          $dat = Song::get_metadata($t);
          $dat["prev"] = $prev->html($album, "<<");
          Song::set_metadata($t, $dat);
        }

      }  // foreach $song

    }  // foreach $album

    $this->nb = $nb;
    $this->tot = $tot;

  }
  

  function mmark($include_name=true) {

    $txt = "";

    if ($include_name) {

      $txt .= $this->name;
      // à améliorer...
      $first = true;
      if (isset($this->q)) {
        $txt .= " {" . $this->q . "}";
        $first = false;
      }
      if (isset($this->nat)) {
        if ($first) $txt .= " ";
        $txt .= "{" . $this->nat . "}";
      }
      $txt .= "\n\n";

    }
   
    if (isset($this->href)) {

      foreach ($this->href as $tit => $url) {
        $txt .= "@ $tit {" . $url . "}\n";
      }
      $txt .= "\n";
      
    }

    if (isset($this->ref)) {

      foreach ($this->ref as $t) {
        
        $txt .= "> " . $t . "\n";
        
      }
     
      $txt .= "\n";

    }

    foreach ($this->disco as $alb) $txt .= $alb->mmark();

    return $txt;
   
  }
  
  function top($t, $hits, $pond = FALSE, $art = TRUE) {

    if ($pond) $hits = number_format($hits, 1, ",", "");

    return '  <li>'.lien($t, $art)." <small>$hits</small></li>\n";

  }

  function is_empty() {

    return count($this->disco) == 1 && count($this->disco[0]) == 0;

  }


  static function delete($t) {

    global $artists;
    unset($artists[$t]);
    $artists->save();
    minilog("delete artist", $t);

  }

}



?>
