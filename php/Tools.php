<?php


function q_encode($input, $the = FALSE) {

 if ($the === "album" && $i = strpos($input, "(")) $input = substr($input, 0, $i);

 $old = array("$", "&", " ! ", " ? ", " + ", " = ", "/", ": ", " ", "'_", "_'", "'", "º", ":", ".", ",", "!", "?", "&_", "__", "#", '"', '+', '*', '#', "¡");
 $neu = array("S", "_", "_", "_", "_", "_", "_", "_", "_", "_", "_", "_", "_");

 return stripslashes(trim( str_replace($old, $neu, accents($input)), "_"));

}


function accents($input) {

 $old = array("À", "Â", "à", "á", "â", "Ç", "ç", "É", "é", "È", "è", "ë", "ê", "î", "ï", "í", "ó", "ô", "Ö", "ö", "û", "ú", "ù", "ü", "ÿ");
 $neu = array("A", "A", "a", "a", "a", "C", "c", "E", "e", "E", "e", "e", "e", "i", "i", "i", "o", "o", "O", "o", "u", "u", "u", "u", "y");

 return str_replace($old, $neu, $input ?? "");

}


function save($cont, $fich, $dir = "data") {

 if (strpos($fich, "__")) $dir = SONG_DIR;
 $ext = substr($fich,-3);
 $mod = ($ext == "log") ? "a" : "w";
 $ser = ($ext == "dat");

 if ($cont) {  // précaution

  $f = fopen("$dir/$fich", $mod);
  flock($f, LOCK_EX);
  fwrite($f, ($ser) ? serialize($cont) : $cont);
  fclose($f);

 } else error("Attention: tentative de sauvegarde de fichier vide");

}


function prepo( $name ) {
  
  foreach (array("The ", "Les ", "La ", "Le ") as $pre)
      if (strpos($name ?? "", $pre) === 0) $name = substr($name, 3).", ".$pre;

  return $name;
  
}


function read($fich, $dir="data") {

 if (strpos($fich, "__")) $dir = SONG_DIR;
 if (file_exists("$dir/$fich")) {
  $f = file_get_contents("$dir/$fich");
  return (substr($fich,-3) == "dat") ? unserialize($f) : $f;
 } else return Null;

}


function html_fragment($name, $repl = array()) {
  
  $html = file_get_contents("html/$name.html");

  foreach( $repl as $symbol => $content ) {
    $ripl = isset($content) ? $content : "";
    $html =  str_replace('{{' . $symbol . '}}', $ripl, $html);
  }

  return $html;
  
}


function message($txt, $type = "notify") {

  global $message;

  $message = '<div id="' . $type . '">' . $txt . '</div>';
  
}


function error($txt) {
  
  message($txt, "error");
  
}


function print_d($ob, $msg="") {
  
  if (superuser()) {
    echo '<pre class="debug">Débug ' . $msg . ': ';
    print_r($ob);
    echo '</pre>';
  }

}


function art_cmp($a, $b) {
  $aa = strtolower(q_encode($a->sortname()));
  $bb = strtolower(q_encode($b->sortname()));
  return strcasecmp($aa, $bb);
}


function minilog($q, $type) {

 $tmp = nom();

 if ($tmp && check($tmp)) {
  $users = read("users.dat");
  $users[$tmp]["last"] = time();
  save($users, "users.dat");
 }

 if ($type == "* FUCK *")  // user-supplied chains
  $q = htmlspecialchars($q);

 if (($type != "toune" || $tmp)) {
  $name = ($tmp) ? " ($tmp)" : "";
  save("[".time()."] $type$name : ".$q."\n", "mini.log");
 }

}


function minilogOTD() {

 $res = "";
 $purged = "";

 $f = fopen("data/mini.log","r");

 while(!feof($f)) {
  $line = fgets($f);
  if ($line) {
   $t = substr($line, 1, 10);
   $m = substr($line, 13);
   if ($t > strtotime("- 1 day")) {
    $res = "[" . date("H:i:s",$t) . "] " . $m  . $res;
    $purged .= $line;
   }
  }
 }

 fclose($f);

 file_put_contents("data/mini.log", $purged);

 return $res;  // tmp

}

function get_IP() {

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, "http://ipecho.net/plain");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $res = curl_exec($ch);
  curl_close($ch);
  return $res;

}


function clickable( $txt ) {  // makes hyperlinks clickables

  $txt = " " . htmlspecialchars($txt);

  $start = strpos($txt, "http");

  while ($start) {

    $stop = strpos($txt, " ", $start);
    if (!$stop) $stop = strlen($txt);

    $url = substr($txt, $start, $stop-$start);

    $repl = '<a href="' . $url . '">' . $url . '</a>';

    $txt = str_replace($url, $repl, $txt);

    $start = strpos($txt, "http", $start + strlen($repl));

  }

  return $txt;

}


function time_f($t, $format = "long") {
  
  $parts = explode("-", $t);  // Y, m, d
  
  $Y = substr($parts[0], 2);
  $m = $parts[1]; if ($m[0] == "0") $m = $m[1];
  $d = $parts[2]; if ($d[0] == "0") $d = $d[1];

  $res = "$d/$m";

  if ($format == "long") $res .= "/$Y";
  
  return $res;
  
}


function increment(&$array, $key, $amount=1) {

  if (!isset($array[$key])) $array[$key] = 0;
  $array[$key] += $amount;

}

function maybe($x) {

  return isset($x) ? $x : NULL;

}

function maybe_ar($tab, $x) {

  return isset($tab[$x]) ? $tab[$x] : NULL;

}

function maybe_cl($obj, $x) {

  return isset($obj->$x) ? $obj->$x : NULL;

}

function valid($t) {

  $mini_chars = "@abcdefghijklmnopqrstuvwxyz0123456789_-()";

  foreach(mb_str_split($t) as $char)

    if (strpos($mini_chars, $char) == false)

      return 1;

  return 0;

}


function rempl_alt($txt) {

  $bef = Array("#"); $aft = Array("♯");

  $delims = Array(" ", "(", ")", "°", "\r", "2", "5", "7", "9", "^", "!", "m", "/", "\n", ",");

  $notes = Array("A", "B", "C", "D", "E", "F", "G", "a", "b", "c", "d", "e", "f", "g");

  foreach( $delims as $x )

    foreach( $notes as $y ) {

      foreach( Array( Array("b", "♭"), Array("x", "𝄪")) as $z)

        foreach( $delims as $w) {

          $bef[] = $x . $y . $z[0] . $w;
          $aft[] = $x . $y . $z[1] . $w;

        }

    }

  foreach( Array("5", "9") as $d ) {

    $bef[] = "(b" . $d;
    $aft[] = "(♭" . $d;

  }

  return substr(str_replace($bef, $aft, " " . $txt),1); // newline hack

}

?>
