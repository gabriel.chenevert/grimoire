<?php

// *********************************************************************
//
//                             SongList
//
// somewhere between an Album & Artist
//
// members:
//
//  str title
//
// methods:
//
//  __construct
//  void edit(str $q)
//  str html()
//  str mmark()
//
// *********************************************************************

class SongList extends SongCollection {

  function __construct($mmark) {

    foreach (explode("\n", $mmark) as $line) if ($line){

      if ($line[0] == "*")  // title

        $this->title = substr($line, 2);

      elseif (trim($line)) {  // song

        $raw = array();
        $mcx = explode(" - ", stripslashes($line));

        $art = $mcx[0];
        if (strpos($art, ". ")) $art = substr($art, strpos($art, ". ") + 2);
        if (strpos($art, "{")) {
            $real_art = substr($art, strpos($art, "{")+1, -1);
            $art = trim(substr($art, 0, strpos($art, "{")));
            $raw["real_artist"] = $real_art;
        }

        $raw["art"] = $art;

        $title = explode("{", $mcx[1]);

        $raw["tit"] = trim($title[0]);

        if (isset($title[1])) $raw["real_title"] = substr(trim($title[1]), 0, -1);

        $this[] = new Song($raw);

      }

    }

  }

  function html($q, $p) {

    $nb_per_column  = 25;
    $nb_columns     = 2;
    $nb_per_page    = $nb_columns * $nb_per_column;

    $nb = count($this);
    $nb_pages = ceil($nb/$nb_per_page);

    $html = "<center><h2>" . $this->title . "</h2>" . barre($this->nb()/$nb) . SPC . SPC;

    if ($nb_pages > 1) for ($i = 1; $i <= $nb_pages; $i++) {
     $html .= "<a href=\"?q=$q&p=$i\">$i</a>";
     if ($i < $nb_pages) $html .= " - ";
    }

    $html .= "</center><br>";
    $html .= "<div class=\"acc\"><ol start=" . ($nb_per_page * ($p-1) + 1) . ">" . ENDL;

    for ($n = ($p-1) * $nb_per_page; $n < $p * $nb_per_page && $n < $nb; $n++) {
     if ($n == $nb_per_page * ($p-1) + $nb_per_column)
       $html .= "</ol></div><ol start=" . (($p-1) * $nb_per_page + $nb_per_column + 1) . ">" . ENDL;
     $html .= $this[$n]->html();
    }

    $html .= "</ol>" . ENDL;

    return $html;

  }

  function edit($q) {

    $repl = array( "Q" => $q, "MMARK" => $this->mmark() );

    return html_fragment("edit_list", $repl);

  }
  
  function mmark() {
  
    $txt = "* " . $this->title . ENDL . ENDL;

    for($i = 0; $i < count($this); $i++)
      $txt .= $this[$i]->mmark("list", $i+1);
    
    return $txt;
    
  }

  function sort() { }

   

}
