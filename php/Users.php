<?php


function N() {

 global $users;
 if (nom()) { $n = $users[nom()]["N"]; return ($n == "∞") ? 50000 : $n; }
 else return 25;

}


function necro() {

  global $users;
  if (nom()) {
	return $users[nom()]["ord"] == "necro";  
  } else return false;
	
}


function superuser($gab = FALSE) {

 $nom = nom();
 if ($nom) {
  if ($gab) return ($nom == "Gab");
  else return ($nom == "Gab" || $nom == "hug" || $nom == "Kri");
 } else return FALSE;

}


function img($usr = NULL) {

 if (!$usr) $usr = nom();
 global $users;
 if (isset($users[$usr]["img"])) {
  $suff = $users[$usr]["img"];
  $nom = (isset($users[$usr]["nom"])) ? $users[$usr]["nom"] : $usr;
  $nom = accents($nom);
  return $nom.".".$suff;
 } else return "default.jpg";

}


function set_unread_messages($usr, $n) {

 global $users;
 $users[$usr]["unread"][] = $n;
 save($users, "users.dat");

}


function unset_unread_messages($usr) {

 global $users;
 $users[$usr]["unread"] = array();
 save($users, "users.dat");

}


function has_unread_messages($usr) {

 global $users;
 if (isset($users[$usr]["unread"]) )
   return $users[$usr]["unread"];
 else
   return False;

}


?>
