<?php

// *********************************************************************
//
//                               Chat
//
// members:
//
//  str title
//
// methods:
//
//  void edit(str $q)
//  str html()
//  str mmark()
//  int latest_reply(int $n)
//
// *********************************************************************

class Chat extends GenericCollection {

  function html($all = false) {
    
    $html = "";

    if (superuser()) {
    
      if (isset($this[0]))
        $html = $this->msg_html(0,0,$all);
      else
        $html = "<i>pas de messages pour l'instant</i>";

    $repl["TITLE"] = $this->title;

    if ($all)
      $repl["LINK"] = '<a href="?q=chatte">Afficher seulement les messages récents</a>';
    else
      $repl["LINK"] = '<a href="?q=chatte&s=all">Afficher tous les messages</a>';

    $html = html_fragment("chat", $repl) . $html;

    } else error("superuser only");
    
    unset_unread_messages(nom());
    
    return $html;

  }
  

  function edit() { }

  function mmark() { }

  function sort() { }
  
  function msg_html($n, $level, $all) {
      
    $msg = $this[$n];
  
    $html = "";

    if (isset($msg["msg"])) {
  
      $tit = ' title="' . date("Y/m/d H:i", $msg["time"]) . '"';
      $id = $msg["id"];
      $txt = $msg["msg"];
      
      $f = maybe_ar($msg, "fich");
      
      if ($f) {
        
        $ff = "media/chat/" . $f;
        
        $ext = explode(".",$f)[1];

        if (file_exists($ff)) {

          if ($ext == "jpg" || $ext == "png" || $ext == "jfif")
            $f = "<img src=\"" . $ff . "\"></img>";
          else
            $f = "<a href=\"" . $ff . "\">$f</a>";
          
        } else $f = "<i>$f</i>";
        
        
      } else $f = "";
  
    } else {
    
      $tit = "";
      $id = "nouveau fil";
      $txt = "";
      $f = "";
      
    }
  
    $repl = Array( "N" => $n, "TIT" => $tit, "MARGIN" => 20 * $level, "ID" => $id, "TXT" => clickable($txt), "FICH" => $f );

    global $users;

    $repl["B"] = (in_array($n, $users[nom()]["unread"])) ? ' class="bold"' : "";

    if (!isset($msg["time"]) || $all || $this->latest_reply($n) > strtotime("-1 month")) {

      $html .= html_fragment("chat_msg", $repl);
    
      if (isset($msg["replies"])) {
    
        // $replies = $msg["replies"];
      
        //if ($n == 0) $replies = array_reverse($replies);
      
        foreach(array_reverse($msg["replies"]) as $m)
        
          $html .= $this->msg_html($m,$level+1,$all);
        

      }

    }
    
    return $html;
  
  }

  function prev($n) {
    
    foreach($this as $m => $msg)
      if (isset($msg["replies"]) && in_array($n, $msg["replies"]))
        return $m;
    
  }

  function bump($n) {
    
    if ($n) {
      $p = $this->prev($n);
      $k = array_search($n, $this[$p]["replies"]);
      unset($this[$p]["replies"][$k]);
      $this[$p]["replies"][] = $n;

      $this->bump($p);
    }
  }

  function latest_reply($n) {

    $r = $this[$n]["time"];

    if (isset($this[$n]["replies"])) foreach($this[$n]["replies"] as $m) {

      $r = max($r, $this->latest_reply($m));

    }

    return $r;

  }

}
