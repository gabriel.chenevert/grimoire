# Le Grimoire du f'n plaisir

Depuis 2002

N'est versionné que le code et non les infos de fonctionnement :

- `backup`
- `data`
- `html/msg`
- `img/pochettes`
- `songs`

Utiliser pour ça les backups. Ou alors on met tout sur git ?
